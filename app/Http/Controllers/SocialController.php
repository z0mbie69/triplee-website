<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Social;

class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $social = Social::all(); 
        return view('backend.social.index')->withsocial($social);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.social.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $social = new Social;
        $social->facebook_url= $request->facebook_url;
        $social->instagram_url= $request->instagram_url;
        $social->twitter_url= $request->twitter_url;
        $social->youtube_url= $request->youtube_url;
        $social->github_url= $request->github_url;

        $social->save();
        return redirect()->route('social.index')->with('flash_message','socials added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $social = Social::find($id);
        return view('backend.social.edit')->with('social',$social); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $social = Social::find($id);
        $social->facebook_url= $request->facebook_url;
        $social->instagram_url= $request->instagram_url;
        $social->twitter_url= $request->twitter_url;
        $social->youtube_url= $request->youtube_url;
        $social->github_url= $request->github_url;

        $social->save();
        return redirect()->route('social.index')->with('flash_message','socials added');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $social=Social::find($id);
        $social->delete();
        return redirect()->route('social.index');
    }
}
