<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logo;
use Storage;

class LogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logo = Logo::all(); 
        return view('backend.logo.index')->withlogo($logo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.logo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $logo= new Logo;

        if ($request->hasFile('limage')){
            $image=$request->file('limage');
            $filename=time().'.'.$image->getClientOriginalExtension();
            $image->move('backend/uploads/image/',$filename);
          
            $logo->limage=$filename;

   
        }
        $logo->name=$request->name;
        $logo->save();
        return redirect()->route('home')->with('flash_message','logo updated');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $logo = logo::find($id);
        return view('backend.logo.edit')->withlogo($logo); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $logo = Logo::find($id);

        if($request->hasFile('limage')){
            $oldfilename=$logo->image;
            //add new photo
            $image=$request->file('limage');
            $filename=time().'.'.$image->getClientOriginalExtension();
            // $location=public_path('/backend/uploads/image/'.$filename);
            // Image::make($image)->resize(80,90)->save($location);
            $image->move('backend/uploads/image/',$filename);
            //update new photo
            $logo->limage=$filename;
              //delete old photo
            Storage::delete($oldfilename);
            // @unlink('public/uploads/products/'.$oldfilename);
            // File::delete($oldfilename); 

          
        } 
        $logo->name=$request->name;
        $logo->save();
        return redirect()->route('logo.index')->with('flash_message','logo updated');  
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
