<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use Image;
use Storage;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = Blog::all(); 
        return view('backend.blog.index')->withblog($blog);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $blog= new Blog;

        if ($request->hasFile('bimage')){
            $image=$request->file('bimage');
            $filename=time().'.'.$image->getClientOriginalExtension();
            $image->move('backend/uploads/image/',$filename);
          
            $blog->bimage=$filename;
        }

        
        $blog->image_title=$request->image_title;
        $blog->image_description=$request->image_description;
        $blog->title=$request->title;
        $blog->description=$request->description;
        $blog->category=$request->category;
        $blog->save();
        return redirect()->route('blog.index')->with('flash_message','blog added'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('backend.blog.edit')->withblog($blog); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog = Blog::find($id);

        if($request->hasFile('bimage')){
            $oldfilename=$blog->image;
            //add new photo
            $image=$request->file('bimage');
            $filename=time().'.'.$image->getClientOriginalExtension();
            // $location=public_path('/backend/uploads/image/'.$filename);
            // Image::make($image)->resize(80,90)->save($location);
            $image->move('backend/uploads/image/',$filename);
            //update new photo
            $blog->bimage=$filename;
              //delete old photo
            Storage::delete($oldfilename);
            // @unlink('public/uploads/products/'.$oldfilename);
            // File::delete($oldfilename); 
        }  
        // dd($request->category);
        $blog->image_title=$request->image_title;
        $blog->image_description=$request->image_description;
        $blog->title=$request->title;
        $blog->description=$request->description;
        $blog->category=$request->category;
        $blog->save();
        return redirect()->route('home')->with('flash_message','Blog updated'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog=Blog::find($id);
        $blog->delete();
        return redirect()->route('blog.index');
    }
}
