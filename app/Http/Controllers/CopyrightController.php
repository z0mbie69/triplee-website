<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Copyright;

class CopyrightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $copyright = Copyright::all(); 
        return view ('backend.copyright.index')->withcopyright($copyright);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.copyright.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $copyright= new Copyright;
        $copyright->name=$request->name;
        $copyright->year=$request->year;
        $copyright->save();
        return redirect()->route('copyright.index')->with('flash_message','copyright added'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $copyright = Copyright::find($id);
        return view('backend.copyright.edit')->withcopyright($copyright);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $copyright = Copyright::find($id);
        $copyright->name=$request->name;
        $copyright->year=$request->year;
        $copyright->save();
        return redirect()->route('copyright.index')->with('flash_message','copyright added'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $copyright=Copyright::find($id);
        $copyright->delete();
        return redirect()->route('copyright.index');
    }
}
