<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Featuredcontent;
use Image;
use Storage;

class FeaturedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $featuredcontent = Featuredcontent::all(); 
        return view('backend.featuredcontent.index')->withfeaturedcontent($featuredcontent);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
   return view('backend.featuredcontent.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $featuredcontent= new Featuredcontent;

        if ($request->hasFile('fimage1')){
            $image=$request->file('fimage1');
            $filename=time().'1'.'.'.$image->getClientOriginalExtension();
            // $location=public_path('/backend/uploads/image/'.$filename);
            // Image::make($image)->resize(80,90)->save($location);
            $featuredcontent->fimage1=$filename;
            $image->move('backend/uploads/image/',$filename);
        }


        
        $featuredcontent->label1=$request->label1;
        $featuredcontent->description1=$request->description1;

        
        $featuredcontent->save();

        return redirect()->route('featured.index')->with('flash_message','content added'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $featuredcontent = Featuredcontent::find($id);
        return view('backend.featuredcontent.edit')->with('featuredcontent',$featuredcontent); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $featuredcontent = Featuredcontent::find($id);
        if($request->hasFile('fimage1')){
            $oldfilename=$featuredcontent->image;
            //add new photo
            $image=$request->file('fimage1');
            $filename=time().'1'.'.'.$image->getClientOriginalExtension();
            // $location=public_path('/backend/uploads/image/'.$filename);
            // Image::make($image)->resize(80,90)->save($location);
            $image->move('backend/uploads/image/',$filename);
          
            //update new photo
            $featuredcontent->fimage1=$filename;
              //delete old photo
            Storage::delete($oldfilename);
            // @unlink('public/uploads/products/'.$oldfilename);
            // File::delete($oldfilename); 
        }
        $featuredcontent->description1=$request->description1;
        $featuredcontent->label1=$request->label1;

            //for image 2

    //         if($request->hasFile('fimage2')){
    //             $oldfilename=$featuredcontent->image;
    //             //add new photo
    //             $image=$request->file('fimage2');
    //             $filename=time().'2'.'.'.$image->getClientOriginalExtension();
    //             // $location=public_path('/backend/uploads/image/'.$filename);
    //             // Image::make($image)->resize(80,90)->save($location);
    //             $image->move('backend/uploads/image/',$filename);
              
    //             //update new photo
    //             $featuredcontent->fimage2=$filename;
    //               //delete old photo
    //             Storage::delete($oldfilename);
    //             // @unlink('public/uploads/products/'.$oldfilename);
    //             // File::delete($oldfilename); 
    //     }  
    //     $featuredcontent->description2=$request->description2;
    //     $featuredcontent->label2=$request->label2;

    //     if($request->hasFile('fimage3')){
    //         $oldfilename=$featuredcontent->image;
    //         //add new photo
    //         $image=$request->file('fimage3');
    //         $filename=time().'3'.'.'.$image->getClientOriginalExtension();
    //         // $location=public_path('/backend/uploads/image/'.$filename);
    //         // Image::make($image)->resize(80,90)->save($location);
    //         $image->move('backend/uploads/image/',$filename);
          
    //         //update new photo
    //         $featuredcontent->fimage3=$filename;
    //           //delete old photo
    //         Storage::delete($oldfilename);
    //         // @unlink('public/uploads/products/'.$oldfilename);
    //         // File::delete($oldfilename); 
    // }        
    //     $featuredcontent->description3=$request->description3;      
    //     $featuredcontent->label3=$request->label3;
        $featuredcontent->save();
        return redirect()->route('featured.index')->with('flash_message','updated'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $featuredcontent=Featuredcontent::find($id);
        $featuredcontent->delete();
        return redirect()->route('featured.index');    }
}
