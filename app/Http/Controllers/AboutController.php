<?php

namespace App\Http\Controllers;
use App\Aboutpara;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function paraindex(){
        $aboutpara = Aboutpara::all();
        return view('backend.aboutpage.aboutpara.index')->withaboutpara($aboutpara);
    }
    public function paracreate(){
        return view('backend.aboutpage.aboutpara.create');
    }
    public function parastore(Request $request){
        $aboutpara = new Aboutpara;
        $aboutpara->category = $request->category;
        $aboutpara->detail =$request->detail;
        $aboutpara->save();
        return redirect()->route('aboutpara.index');
    }
    public function paraedit($id){
        $aboutpara = Aboutpara::find($id);
        return view('backend.aboutpage.aboutpara.edit')->withaboutpara($aboutpara);
    }
    public function paraupdate(Request $request,$id){
        $aboutpara = Aboutpara::find($id);
        $aboutpara->category = $request->category;
        $aboutpara->detail =$request->detail;
        $aboutpara->save();
        return redirect()->route('aboutpara.index');

    }
    public function paradestroy($id){
        $aboutpara = Aboutpara::find($id);
        $aboutpara->delete();
        return redirect()->route('aboutpara.index');
    }
}
