<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $slider =Slider::all();
        return view('backend.slider.index')->withslider($slider);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('backend.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slider = new Slider;
        // if ($request->hasFile('simage')){
        //     $image=$request->file('simage');
        //     $filename=time().'.'.$image->getClientOriginalExtension();
        //     $location=public_path('/backend/uploads/image/'.$filename);
        //     Image::make($image)->resize(80,90)->save($location);
        //     $slider->simage=$filename;
           
        // }
        $slider->stitle=$request->stitle;
        $slider->sdetail=$request->sdetail;
        $slider->i1name=$request->i1name;
        $slider->i1detail=$request->i1detail;
        $slider->i2name=$request->i2name;
        $slider->i2detail=$request->i2detail;
        $slider->i3name=$request->i3name;
        $slider->i3detail=$request->i3detail;
        $slider->save();
        return redirect()->route('slider.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $slider =Slider::find($id);
       return view('backend.slider.edit')->withslider($slider);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = Slider::find($id);
        // if ($request->hasFile('simage')){
        //     $image=$request->file('simage');
        //     $filename=time().'.'.$image->getClientOriginalExtension();
        //     $location=public_path('/backend/uploads/image/'.$filename);
        //     Image::make($image)->resize(80,90)->save($location);
        //     $slider->simage=$filename;
           
        // }
        $slider->stitle=$request->stitle;
        $slider->sdetail=$request->sdetail;
        $slider->i1name=$request->i1name;
        $slider->i1detail=$request->i1detail;
        $slider->i2name=$request->i2name;
        $slider->i2detail=$request->i2detail;
        $slider->i3name=$request->i3name;
        $slider->i3detail=$request->i3detail;
        $slider->save();
        return redirect()->route('slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider=slider::find($id);
        $slider->delete();
        return redirect()->route('slider.index');
    }
}
