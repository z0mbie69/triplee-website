<?php

namespace App\Http\Controllers\about;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Numeric;
class NumericController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $numeric = Numeric::all();
        return view('backend.aboutpage.numeric.index')->withnumeric($numeric);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('backend.aboutpage.numeric.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $numeric = new Numeric;
        $numeric->title =$request->title;
        $numeric->icon = $request->icon;
        $numeric->number = $request->number;
        $numeric->save();
        return redirect()->route('numeric.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $numeric =  Numeric::find($id);
        return view('backend.aboutpage.numeric.edit')->withnumeric($numeric);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $numeric =  Numeric::find($id);
        $numeric->title =$request->title;
        $numeric->icon = $request->icon;
        $numeric->number = $request->number;
        $numeric->save();
        return redirect()->route('numeric.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $numeric =  Numeric::find($id);
        $numeric->delete();
        return redirect()->route('numeric.index');


    }
}
