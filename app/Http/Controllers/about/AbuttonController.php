<?php

namespace App\Http\Controllers\about;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Abutton;
class AbuttonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $abutton = Abutton::all();
        return view('backend.aboutpage.abutton.index')->with('abutton',$abutton);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.aboutpage.abutton.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $abutton= new Abutton;
        $abutton->title = $request->title;
        $abutton->detail = $request->detail;
        $abutton->ticon = $request->ticon;
        $abutton->bicon = $request->bicon;
        $abutton->save();
        return redirect()->route('abutton.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $abutton = Abutton::find($id);
        return view('backend.aboutpage.abutton.edit')->withabutton($abutton);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $abutton = Abutton::find($id);
        $abutton->title = $request->title;
        $abutton->detail = $request->detail;
        $abutton->ticon = $request->ticon;
        $abutton->bicon = $request->bicon;

        $abutton->save();
        return redirect()->route('abutton.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $abutton=Abutton::find($id);
        $abutton->delete();
        return redirect()->route('abutton.index');
    }
}
