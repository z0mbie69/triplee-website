<?php

namespace App\Http\Controllers\about;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Approach;

class ApproachController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $approach = approach::all();
        return view('backend.aboutpage.approach.index')->with('approach',$approach);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.aboutpage.approach.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $approach= new approach;
        $approach->title = $request->title;
        $approach->detail = $request->detail;
        $approach->icon = $request->icon;
        $approach->save();
        return redirect()->route('approach.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $approach = approach::find($id);
        return view('backend.aboutpage.approach.edit')->withapproach($approach);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $approach = approach::find($id);
        $approach->title = $request->title;
        $approach->detail = $request->detail;
        $approach->icon = $request->icon;
        $approach->save();
        return redirect()->route('approach.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $approach=approach::find($id);
        $approach->delete();
        return redirect()->route('approach.index');
    }
}
