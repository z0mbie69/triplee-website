<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mslider;
use Image;
class MsliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $mslider =Mslider::all();
        return view('backend.mslider.index')->withmslider($mslider);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.mslider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mslider = new Mslider;
        if ($request->hasFile('simage')){
            $image=$request->file('simage');
            $filename=time().'.'.$image->getClientOriginalExtension();
            // $location=public_path('/backend/uploads/image/'.$filename);
            // Image::make($image)->resize(80,90)->save($location);
            $image->move('backend/uploads/image/', $filename);

            $mslider->simage=$filename;
           
        }
        // $file->move('public/adminpanel/uploads/profile/', $filename);
        // $user->image = 'public/adminpanel/uploads/profile/'.$filename;
        $mslider->save();
        return redirect()->route('mslider.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mslider =Mslider::find($id);
        return view('backend.mslider.edit')->withmslider($mslider);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mslider = Mslider::find($id);
        if ($request->hasFile('simage')){
            $image=$request->file('simage');
            $filename=time().'.'.$image->getClientOriginalExtension();
            // $location=public_path('/backend/uploads/image/'.$filename);
            // Image::make($image)->resize(80,90)->save($location);
            $image->move('backend/uploads/image/', $filename);

            $mslider->simage=$filename;
           
        }
        $mslider->save();
        return redirect()->route('mslider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mslider=Mslider::find($id);
        $mslider->delete();
        return redirect()->route('mslider.index');
    }
}
