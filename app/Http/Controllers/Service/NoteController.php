<?php

namespace App\Http\Controllers\Service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Note;
use Image;
class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $note = Note::all();
        return view('backend.servicepage.note.index')->with('note',$note);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.servicepage.note.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $note= new Note;
        if ($request->hasFile('image')){
            $image=$request->file('image');
            $filename=time().'.'.$image->getClientOriginalExtension();
            // $location=public_path('/backend/uploads/image/'.$filename);
            // Image::make($image)->resize(80,90)->save($location);
            $image->move('backend/uploads/image/', $filename);

            $note->image=$filename;
           
        }
        $note->detail = $request->detail;
        $note->title= $request->title;
        
        $note->save();
        return redirect()->route('note.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $note = Note::find($id);
        return view('backend.servicepage.note.edit')->withnote($note);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $note = Note::find($id);
            if ($request->hasFile('image')){
            $image=$request->file('image');
            $filename=time().'.'.$image->getClientOriginalExtension();
            // $location=public_path('/backend/uploads/image/'.$filename);
            // Image::make($image)->resize(80,90)->save($location);
            $image->move('backend/uploads/image/', $filename);

            $note->image=$filename;
           
        }
        $note->detail = $request->detail;
        $note->title= $request->title;
        $note->save();
        return redirect()->route('note.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note=Note::find($id);
        $note->delete();
        return redirect()->route('note.index');
    }
}
