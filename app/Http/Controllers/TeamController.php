<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use session;
use Image;
use Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $team = Team::all(); 
        return view('backend.team.index')->withteam($team);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $team= new Team;

        if ($request->hasFile('timage')){
            $image=$request->file('timage');
            $filename=time().'.'.$image->getClientOriginalExtension();
            $image->move('backend/uploads/image/',$filename);
          
            $team->timage=$filename;
        }

        
        $team->name=$request->name;
        $team->description=$request->description;
        $team->category=$request->category;
        $team->facebook_url=$request->facebook_url;
        $team->twitter_url=$request->twitter_url;
        $team->instagram_url=$request->instagram_url;
        $team->save();
        return redirect()->route('team.index')->with('flash_message','image added'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = Team::find($id);
        return view('backend.team.edit')->with('team',$team); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $team = Team::find($id);
       
        if($request->hasFile('bimage')){
            $oldfilename=$team->image;
            //add new photo
            $image=$request->file('timage');
            $filename=time().'.'.$image->getClientOriginalExtension();
            // $location=public_path('/backend/uploads/image/'.$filename);
            // Image::make($image)->resize(80,90)->save($location);
            $image->move('backend/uploads/image/',$filename);
            //update new photo
            $team->timage=$filename;
              //delete old photo
            Storage::delete($oldfilename);
            // @unlink('public/uploads/products/'.$oldfilename);
            // File::delete($oldfilename); 
        }  

        $team->name=$request->name;
        $team->description=$request->description;
        $team->category=$request->category;
        $team->facebook_url=$request->facebook_url;
        $team->twitter_url=$request->twitter_url;
        $team->instagram_url=$request->instagram_url;
        $team->save();
        return redirect()->route('team.index')->with('flash_message','Team updated'); 
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team=Team::find($id);
        $team->delete();
        return redirect()->route('team.index');
    }
}
