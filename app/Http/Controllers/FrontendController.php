<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alltitle;
use App\Service;
use App\Team;
use App\Blog;
use App\Slider;
use App\Featuredcontent;
use App\Contact;
use App\Mslider;
use App\Aboutpara;
use App\Numeric;
use App\Approach;
use App\Abutton;
use App\Note;
use App\Value;
use Mail;
use Session;
class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    
    public function index()
    {
$featuredcontent=Featuredcontent::all();
        $team = Team::all(); 
        $service = Service::latest()->get();
        $slider = Slider::latest()->get();
        $mslider = Mslider::latest()->take(3)->get();

        return view('frontend.dashboard')->withservice($service)->withslider($slider)->
        withteam($team)->withfeaturedcontent($featuredcontent)->withmslider($mslider);      
        
    }
    public function featureShow($id){
        $featuredcontent = Featuredcontent::find($id);
        $featured2 = Featuredcontent::latest()->take(5)->get();
      //   $blogsh = Blog::find($id);
        return view('frontend.pages.featuredcontentdetail')->withfeaturedcontent($featuredcontent)->withfeatured2($featured2);
      }
        
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getAbout(){
        $aboutpara1 = Aboutpara::where('category','aboutparagraph')->latest()->get();
        $aboutpara2 = Aboutpara::where('category','visionparagraph')->latest()->get();
        $numeric = Numeric::latest()->get();
        $abouttitle = Alltitle::where('category','About')->latest()->get();
        $approach = Approach::latest()->get();
        $abutton = Abutton::latest()->get();
        $service = Service::latest()->get();
        $slider = Slider::latest()->get();
        $team = Team::all();
        return view('frontend.pages.about')->withservice($service)->withslider($slider)
        ->withabouttitle($abouttitle)
        ->withaboutpara1($aboutpara1)
        ->withaboutpara2($aboutpara2)
        ->withnumeric($numeric)
        ->withapproach($approach)
        ->withabutton($abutton)
        ->withteam($team)
        ;
    }
    public function getService(){
        $servicetitle = Alltitle::where('category','Service')->latest()->get();
        $value =Value::latest()->get();
        $note =Note::latest()->get();
        $service = Service::latest()->get();
        $slider = Slider::latest()->get();
        return view('frontend.pages.service')->withservice($service)->withslider($slider)
        ->withservicetitle($servicetitle)->withvalue($value)->withnote($note);
    }  
    public function getPortfolio(){
        $portfoliotitle = Alltitle::where('category','Portfolio')->latest()->get();
        $service = Service::latest()->get();
        $slider = Slider::latest()->get();
        return view('frontend.pages.portfolio')->withservice($service)->withslider($slider)->withportfoliotitle($portfoliotitle);
    }  
    public function getOurteam(){
        $team= Team::all();
        $service = Service::latest()->get();
        $slider = Slider::latest()->get();
        $teamtitle = Alltitle::where('category','Our Team')->latest()->get();
        return view('frontend.pages.ourteam')
        ->withservice($service)
        ->withslider($slider)
        ->withteam($team)
        ->withteamtitle($teamtitle);
    }  
    public function getContact(){
        $service = Service::latest()->get();
        $slider = Slider::latest()->get();
        $contact = Contact::latest()->take(1)->get();
        return view('frontend.pages.contact')->withservice($service)->withslider($slider)->withcontact($contact);
    }  
    public function getBlog(){
        $blogs= Blog::orderBy('id', 'desc')->paginate(3);
        $blog2 = Blog::latest()->take(5)->get();
       
        $service = Service::latest()->get();
        $slider = Slider::latest()->get();
        $blogtitle = Alltitle::where('category','Blog')->latest()->get();
        return view('frontend.pages.blog')->withservice($service)->withslider($slider)->withblogtitle($blogtitle)->withblogs($blogs)
        ->withblog2($blog2);
    }

    public function blogShow($id){
      $blog = Blog::find($id);
      $blog3 = Blog::latest()->take(5)->get();
    //   $blogsh = Blog::find($id);
      return view('frontend.pages.blogpost')->withblog($blog)->withblog3($blog3);
    }

  
    
    public function saveform(Request $request){
      
        $this->validate($request,[
            'email'=>'required|email',
            'msg'=>'min:10',
            'subject'=>'min:3'
        ]);
        $data=array(
            'name'=>$request->name,
            'email' => $request->email,
            'subject'=>$request->subject,
            'bodyMessage'=>$request->msg,
        );
        Mail::send('frontend.email.contact',$data,function($message) use ($data){
            $message->from($data['email']);
            $message->to('eeeinnovation369@gmail.com');
            $message->subject($data['subject']);
        });
        return redirect()->route('front.contact')->with('success','Messege sent Successfully');
    }
}
