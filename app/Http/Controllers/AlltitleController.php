<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alltitle;
class AlltitleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $alltitle =Alltitle::all();
        return view('backend.alltitle.index')->withalltitle($alltitle);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.alltitle.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $alltitle = new Alltitle;
        $alltitle->title =$request->title;
        $alltitle->detail = $request->detail;
        // if ($request->hasFile('cimage')){
        //     $image=$request->file('cimage');
        //     $filename=time().'.'.$image->getClientOriginalExtension();
        //     $location=public_path('/backend/uploads/image/'.$filename);
        //     Image::make($image)->resize(80,90)->save($location);
        //     $alltitle->cimage=$filename;
           
        // }
        $alltitle->category =$request->category;
        $alltitle->save();
        return redirect()->route('alltitle.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $alltitle = Alltitle::find($id);
        return view('backend.alltitle.edit')->withalltitle($alltitle);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $alltitle = Alltitle::find($id);
        $alltitle->title =$request->title;
        $alltitle->detail = $request->detail;
        $alltitle->category =$request->category;

        // if ($request->hasFile('cimage')){
        //     $image=$request->file('cimage');
        //     $filename=time().'.'.$image->getClientOriginalExtension();
        //     $location=public_path('/backend/uploads/image/'.$filename);
        //     Image::make($image)->resize(80,90)->save($location);
        //     $alltitle->cimage=$filename;
           
        // }
        $alltitle->save();
        return redirect()->route('alltitle.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $alltitle=alltitle::find($id);
        $alltitle->delete();
        return redirect()->route('alltitle.index');
    }
}
