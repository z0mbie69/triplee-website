<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use View;
use App\Social;
use App\Contact;
use App\Logo;
use App\Copyright;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view::composer(['*'], function($view){

            $view->with('social_data',Social::whereId(1)->first());
        });

        view::composer(['*'], function($view){

            $view->with('contact_data',Contact::whereId(1)->first());
        });

        view::composer(['*'], function($view){

            $view->with('logo_data',Logo::whereId(1)->first());
        });

    }
}
