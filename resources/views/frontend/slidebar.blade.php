<div class="ms-slidebar sb-slidebar sb-left sb-style-overlay" id="ms-slidebar">
    <div class="sb-slidebar-container">
      <header class="ms-slidebar-header">
        <!--
        <div class="ms-slidebar-login">
          <a href="javascript:void(0)" class="withripple"><i class="fas fa-user"></i> Login</a>
          <a href="javascript:void(0)" class="withripple"><i class="fas fa-account-add"></i> Register</a>
        </div>-->
        <div class="ms-slidebar-title">
          <form class="search-form">
            <input id="search-box-slidebar" type="text" class="search-input" placeholder="Search..." name="q" />
            <label for="search-box-slidebar"><i class="fas fa-search"></i></label>
          </form>
          <div class="ms-slidebar-t">
          <a href="{{Route('front.index')}}">
          <img src="{{asset('backend/uploads/image/'.$logo_data->limage)}}" style="width:50px; height:50px; border-radius:100px!important;"  >

            <h3>EEE<span class="innovation">Innovation</span></h3> </a>
          </div>
        </div>
      </header>
      <ul class="ms-slidebar-menu" id="slidebar-menu" role="tablist" aria-multiselectable="true">
        <li class="card">
          <a class="collapsed"  href="/"  >
            <i class="fas fa-home"></i> Home </a>
        </li>
        <li class="card">
          <a class="collapsed"  href="/about"  >
          <i class="fas fa-question"></i> About Us </a>
        </li>
        <li class="card">
          <a class="collapsed"  href="/service"  >
          <i class="fas fa-concierge-bell"></i> Services </a>
        </li>
        <li class="card">
          <a class="collapsed"  href="/portfolio"  >
            <i class="fas fa-id-card-alt"></i> Portfolio </a> 
        </li>
        <!-- <li class="card">
          <a class="collapsed"  href="Fourteam"  >
            <i class="fas fa-home"></i> Our Team </a>
        </li> -->
        <li class="card">
          <a class="collapsed"  href="/contact"  >
            <i class="fas fa-id-card"></i> Contact </a>
        </li>
        <li class="card">
          <a class="collapsed"  href="/blog"  >
            <i class="fas fa-blog"></i> Blog </a>
        </li>
      </ul>

    </div>
  </div>