@if (count($errors)>0)
@foreach ($errors->all() as $error)
<div class="offset-1 col-md-7">
        <div class="content-wrapper">
    <div class="alert alert-danger">
        <div class="text-center">
                {{$error}}

        </div>
    </div>
        </div></div>
@endforeach
    
@endif

@if(session('success'))
<div class="offset-1 col-md-7">
<div class="content-wrapper">
<div class="alert alert-success">
    <div class="text-center">
            {{session('success')}}

    </div>

    </div>

    
</div>
</div>

    
@endif
