<!DOCTYPE html>
<html lang="en">
 @include('frontend.head')
  <body>
    <div id="ms-preload" class="ms-preload">
      <div id="status">
        <div class="spinner">
          <div class="dot1"></div>
          <div class="dot2"></div>
        </div>
      </div>
    </div>
    <div class="ms-site-container">
    @include('frontend.navbar')  
    @include('frontend.message')
    @yield('content')

    @include('frontend.footer')
    </div> <!-- ms-site-container -->
    @include('frontend.slidebar')
    @include('frontend.script')

  </body>
</html>
