@extends('frontend.main')

@section('content')
<div class="ms-hero ms-hero-material">
    <span class="ms-hero-bg"></span>
    <div class="container">
      <div class="row">
        
         

        <div class="col-xl-6 col-lg-7">
          <div id="carousel-hero" class="carousel slide carousel-fade" data-ride="carousel" data-interval="8000">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox"> 
              @foreach ($slider as $slider)
                    
              <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                <div class="carousel-caption">
                  <div class="ms-hero-material-text-container">
                    <header class="ms-hero-material-title animated slideInLeft animation-delay-5">
                      <h1 class="animated fadeInLeft animation-delay-15 font-smoothing"><strong>{{$slider->stitle}}</strong> </h1>
                      <h2 class="animated fadeInLeft animation-delay-18"> <span class="color-warning">{!!$slider->sdetail!!}</span> </h2>
                    </header>
                    <ul class="ms-hero-material-list">
                      <li class="">
                        <div class="ms-list-icon animated zoomInUp animation-delay-18">
                          <span class="ms-icon ms-icon-circle ms-icon-xlg color-warning shadow-3dp"><i class="fas fa-{{$slider->i1name}}"></i></span>
                        </div>
                        <div class="ms-list-text animated fadeInRight animation-delay-19">{!!$slider->i1detail!!}</div>
                      </li>
                      <li class="">
                        <div class="ms-list-icon animated zoomInUp animation-delay-20">
                          <span class="ms-icon ms-icon-circle ms-icon-xlg color-success shadow-3dp"><i class="fas fa-{{$slider->i2name}}"></i></span>
                        </div>
                        <div class="ms-list-text animated fadeInRight animation-delay-21">{!!$slider->i2detail!!}</div>
                      </li>
                      <li class="">
                        <div class="ms-list-icon animated zoomInUp animation-delay-22">
                          <span class="ms-icon ms-icon-circle ms-icon-xlg color-danger shadow-3dp"><i class="fas fa-{{$slider->i3name}}"></i></span>
                        </div>
                        <div class="ms-list-text animated fadeInRight animation-delay-23">{!!$slider->i3detail!!}</div>
                      </li>
                    </ul>
                    <div class="ms-hero-material-buttons text-right">
                      <!-- <div class="ms-hero-material-buttons text-right">
                        <a href="javascript:void(0);" class="btn btn-warning btn-raised animated fadeInLeft animation-delay-24 mr-2"><i class="fas fa-bicycle"></i> Personalize</a>
                        <a href="javascript:void(0);" class="btn btn-success btn-raised animated fadeInRight animation-delay-24"><i class="fas fa-cloud-download-alt"></i> Download</a>
                      </div> -->
                    </div>
                  </div> <!-- ms-hero-material-text-container -->
                </div>
              </div>
              @endforeach

              <div class="carousel-controls">
                <!-- Controls -->
                <a class="left carousel-control animated zoomIn animation-delay-30" href="#carousel-hero" role="button" data-slide="prev">
                  <i class="fas fa-chevron-left"></i>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control animated zoomIn animation-delay-30" href="#carousel-hero" role="button" data-slide="next">
                  <i class="fas fa-chevron-right"></i>
                  <span class="sr-only">Next</span>
                </a>
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#carousel-hero" data-slide-to="0" class="animated fadeInUpBig animation-delay-27 active"></li>
                  <li data-target="#carousel-hero" data-slide-to="1" class="animated fadeInUpBig animation-delay-28"></li>
                  <li data-target="#carousel-hero" data-slide-to="2" class="animated fadeInUpBig animation-delay-29"></li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-xl-6 col-lg-5">
          <div class="desktop_image animation-delay-30">
                  <img src="{{asset('frontend/assets/img/mock-imac-material2.png')}}" alt="" class="img-fluid">
                  <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
                      <div class="carousel-inner">
                        @foreach($mslider as $mslider)
                        <div class="carousel-item {{$loop->first?"active":""}}">
                          <img src="{{asset('backend/uploads/image/'.$mslider->simage)}}"  class="d-block w-100" alt="...">
                        </div>
                        @endforeach
                        
                      
                      </div>
                      <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
              </div>  
            </div>
          </div>
      </div><!--row-->
    </div> <!-- container -->
  </div>
</div>
    
<section class="our-services">
      	<div class="container">
        <section class="mb-4">
          <h2 class="text-center no-mt mb-6 wow fadeInUp">Our Services</h2>
          <div class="row">
            @foreach($service as $service)
            <div class="col-lg-4 col-md-6 col-sm-6 mb-2">
              <div class="ms-icon-feature wow flipInX animation-delay-4">
                <div class="ms-icon-feature-icon">
                  <span class="ms-icon ms-icon-lg ms-icon-inverse"><i class="fa fa-cloud"></i></span>
                </div>
                <div class="ms-icon-feature-content">
                  <h4 class="color-primary">{{substr(strip_tags($service->title),0,22)}}{{strlen($service->title)>15?"...":""}}</h4>
                  <p> {{substr(strip_tags($service->description),0,22)}}{{strlen($service->description)>15?"...":""}}</p>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </section>
      </div> <!-- container -->
</section>
    
    <!-- 
    <section class="our-clients">
              
          
        <div class="container mt-6">
          <h1 class="font-light text-center">Our Clients</h1>
          <p class="lead color-primary text-center">— Intelligent team that help you do your best work. </p>
          <div class="panel panel-light panel-flat">
            Nav tabs 
            <ul class="nav nav-tabs nav-tabs-transparent indicator-primary nav-tabs-full nav-tabs-5" role="tablist">
              <li class="nav-item wow fadeInDown animation-delay-6" role="presentation"><a href="#windows" aria-controls="windows" role="tab" data-toggle="tab" class="nav-link withoutripple"><i class="fas fa-user"></i> <span class="d-none d-md-inline">Windows</span></a></li>
              <li class="nav-item wow fadeInDown animation-delay-4" role="presentation"><a href="#macos" aria-controls="macos" role="tab" data-toggle="tab" class="nav-link withoutripple active"><i class="fas fa-user"></i> <span class="d-none d-md-inline">MacOS</span></a></li>
              <li class="nav-item wow fadeInDown animation-delay-2" role="presentation"><a href="#linux" aria-controls="linux" role="tab" data-toggle="tab" class="nav-link withoutripple"><i class="fas fa-user"></i> <span class="d-none d-md-inline">Linux</span></a></li>
              <li class="nav-item wow fadeInDown animation-delay-4" role="presentation"><a href="#android" aria-controls="android" role="tab" data-toggle="tab" class="nav-link withoutripple"><i class="fas fa-user"></i> <span class="d-none d-md-inline">Android</span></a></li>
              <li class="nav-item wow fadeInDown animation-delay-6" role="presentation"><a href="#ios" aria-controls="ios" role="tab" data-toggle="tab" class="nav-link withoutripple"><i class="fas fa-user"></i> <span class="d-none d-md-inline">IOS</span></a></li>
            </ul>
            <div class="panel-body">
               Tab panes 
              <div class="tab-content mt-4">
                <div role="tabpanel" class="tab-pane fade" id="windows">
                  <div class="row">
                    <div class="col-lg-6 order-lg-2">
                      <img src="{{asset('frontend/assets/img/mock4.png')}}" alt="" class="img-fluid animated zoomIn animation-delay-8">
                    </div>
                    <div class="col-lg-6 order-lg-1">
                      <h3 class="text-normal animated fadeInUp animation-delay-4">Bring ideas together faster</h3>
                      <p class="lead lead-md animated fadeInUp animation-delay-6">Create documents, spreadsheets and presentations from anywhere. Share them with teammates and work together on the same file, at the same time.</p>
                      <p class="lead lead-md animated fadeInUp animation-delay-7">sing your work is easy with one login for everything you do. Administrative controls offer two-step verification to enhance security for the whole company.</p>
                      <div class="">
                        <a href="javascript:void(0)" class="btn btn-info btn-raised animated zoomIn animation-delay-10"><i class="fab fa-readme"></i> More info</a>
                        
                      </div>
                    </div>
                  </div>
                </div>
                <div role="tabpanel" class="tab-pane active show fade" id="macos">
                  <div class="row">
                    <div class="col-lg-6">
                      <img src="{{asset('frontend/assets/img/mock2.png')}}" alt="" class="img-fluid wow animated zoomIn animation-delay-8">
                    </div>
                    <div class="col-lg-6">
                      <h3 class="text-normal wow animated fadeInUp animation-delay-4">Bring ideas together faster</h3>
                      <p class="lead lead-md  wow animated fadeInUp animation-delay-6">Create documents, spreadsheets and presentations from anywhere. Share them with teammates and work together on the same file, at the same time.</p>
                      <p class="lead lead-md wow animated fadeInUp animation-delay-7">sing your work is easy with one login for everything you do. Administrative controls offer two-step verification to enhance security for the whole company.</p>
                      <div class="">
                        <a href="javascript:void(0)" class="btn btn-info btn-raised wow animated zoomIn animation-delay-10"><i class="fab fa-readme"></i> More info</a>
                        
                      </div>
                    </div>
                  </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="linux">
                  <div class="row">
                    <div class="col-lg-6 order-lg-2">
                      <img src="{{asset('frontend/assets/img/mock5.png')}}" alt="" class="img-fluid animated zoomIn animation-delay-8">
                    </div>
                    <div class="col-lg-6 order-lg-1">
                      <h3 class="text-normal animated fadeInUp animation-delay-4">Bring ideas together faster</h3>
                      <p class="lead lead-md animated fadeInUp animation-delay-6">Create documents, spreadsheets and presentations from anywhere. Share them with teammates and work together on the same file, at the same time.</p>
                      <p class="lead lead-md animated fadeInUp animation-delay-7">sing your work is easy with one login for everything you do. Administrative controls offer two-step verification to enhance security for the whole company.</p>
                      <div class="">
                        <a href="javascript:void(0)" class="btn btn-info btn-raised animated zoomIn animation-delay-10"><i class="fab fa-readme"></i> More info</a>
                        
                      </div>
                    </div>
                  </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="android">
                  <div class="row">
                    <div class="col-lg-6">
                      <img src="{{asset('frontend/assets/img/mock6.png')}}" alt="" class="img-fluid animated zoomIn animation-delay-8">
                    </div>
                    <div class="col-lg-6">
                      <h3 class="text-normal animated fadeInUp animation-delay-4">Bring ideas together faster</h3>
                      <p class="lead lead-md  animated fadeInUp animation-delay-6">Create documents, spreadsheets and presentations from anywhere. Share them with teammates and work together on the same file, at the same time.</p>
                      <p class="lead lead-md animated fadeInUp animation-delay-7">sing your work is easy with one login for everything you do. Administrative controls offer two-step verification to enhance security for the whole company.</p>
                      <div class="">
                        <a href="javascript:void(0)" class="btn btn-info btn-raised animated zoomIn animation-delay-10"><i class="fab fa-readme"></i> More info</a>
                        
                      </div>
                    </div>
                  </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="ios">
                  <div class="row">
                    <div class="col-lg-6 order-lg-2">
                      <img src="{{asset('frontend/assets/img/mock3.png')}}" alt="" class="img-fluid animated zoomIn animation-delay-8">
                    </div>
                    <div class="col-lg-6 order-lg-1">
                      <h3 class="text-normal animated fadeInUp animation-delay-4">Bring ideas together faster</h3>
                      <p class="lead lead-md animated fadeInUp animation-delay-6">Create documents, spreadsheets and presentations from anywhere. Share them with teammates and work together on the same file, at the same time.</p>
                      <p class="lead lead-md animated fadeInUp animation-delay-7">sing your work is easy with one login for everything you do. Administrative controls offer two-step verification to enhance security for the whole company.</p>
                      <div class="">
                        <a href="javascript:void(0)" class="btn btn-info btn-raised animated zoomIn animation-delay-10"><i class="fab fa-readme"></i> More info</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> panel 
        </div>  container 
        </section>-->

     
   
      
      
      <section class="type-writer-section ms-hero-bg-dark color-white intro-hero-full ">
          <div class="intro-hero-full-content">
          <div class="container text-center">
              <!--
            <span class="ms-logo ms-logo-lg ms-logo-white center-block mb-2 mt-2 animated zoomInDown animation-delay-7">E</span>
            <h2 class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-7">EEE<span class="innovation">Innovation</span></h2>-->
            <h2 class="animated fadeInUp animation-delay-12"> <span class="typed-class typed-block color-primary"></span><span class="typed-cursor"></span></h2>
          </div>
        </div>
      </section>
      

      <div class="container mt-6" style="height:560px;!important;">
        <h2 class="text-center color-primary mb-4">Featured Content</h2>

        <div class="owl-dots"></div>
        
        <div class="owl-carousel owl-theme">


        @foreach($featuredcontent as $featuredcontent)

          <div class="card animation-delay-6">
            <div class="withripple zoom-img">
              <a href="{{Route('feature.show',$featuredcontent->id)}}"><img src="{{asset('backend/uploads/image/'.$featuredcontent->fimage1)}}   
              "style="height:280px;!important;" alt="..." class="img-fluid"></a>
            </div>
            <div class="card-body">
              <h3 class="color-primary">{{substr(strip_tags($featuredcontent->label1),0,20)}}{{strlen($featuredcontent->label1)>15?"...":""}}</h3>
              <p>{{substr(strip_tags($featuredcontent->description1),0,30)}}{{strlen($featuredcontent->description1)>15?"...":""}}</p>
              <p class="text-right">
                <a href="{{Route('feature.show',$featuredcontent->id)}}" class="btn btn-primary btn-raised text-right" role="button"><i class="fas fa-eye"></i> View More</a>
              </p>
            </div>
          </div>
        
         
          @endforeach
  
        </div>
      </div>
<br>
<br>
<br>

   

  
@endsection