  <!-- Modal -->
  <div class="modal modal-primary" id="ms-account-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog animated zoomIn animated-3x" role="document">
      <div class="modal-content">
        <div class="modal-header d-block shadow-2dp no-pb">
          <button type="button" class="close d-inline pull-right mt-2" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fas fa-window-close"></i></span></button>
          <div class="modal-title text-center">
          <a href="{{Route('front.index')}}">
          <img src="{{asset('backend/uploads/image/'.$logo_data->limage)}}" style="width:50px; height:50px; border-radius:100px!important;"  >
          </a>

          <span class="logo-lg">
        <!-- put image here for logo -->
        <p>{{$logo_data->name}}</p>
      </span>

          </div>
          <div class="modal-header-tabs">
            <ul class="nav nav-tabs nav-tabs-full nav-tabs-3 nav-tabs-primary" role="tablist">
              <li class="nav-item" role="presentation"><a href="#ms-login-tab" aria-controls="ms-login-tab" role="tab" data-toggle="tab" class="nav-link active withoutripple"><i class="fas fa-user"></i> Login</a></li>
              <li class="nav-item" role="presentation"><a href="#ms-register-tab" aria-controls="ms-register-tab" role="tab" data-toggle="tab" class="nav-link withoutripple"><i class="fas fa-user-plus"></i> Register</a></li>
              <li class="nav-item" role="presentation"><a href="#ms-recovery-tab" aria-controls="ms-recovery-tab" role="tab" data-toggle="tab" class="nav-link withoutripple"><i class="fas fa-key"></i> Recovery Pass</a></li>
            </ul>
          </div>
        </div>
        <div class="modal-body">
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade active show" id="ms-login-tab">
              <form autocomplete="off">
                <fieldset>
                  <div class="form-group label-floating">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-user"></i></span>
                      <label class="control-label" for="ms-form-user">Username</label>
                      <input type="text" id="ms-form-user" class="form-control">
                    </div>
                  </div>
                  <div class="form-group label-floating">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-lock"></i></span>
                      <label class="control-label" for="ms-form-pass">Password</label>
                      <input type="password" id="ms-form-pass" class="form-control">
                    </div>
                  </div>
                  <div class="row mt-2">
                    <div class="col-md-6">
                      <div class="form-group no-mt">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox"> Remember Me </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <button class="btn btn-raised btn-primary pull-right">Login</button>
                    </div>
                  </div>
                </fieldset>
              </form>
              <div class="text-center">
                <h3>Login with</h3>
                <a href="javascript:void(0)" class="wave-effect-light btn btn-raised btn-facebook"><i class="fab fa-facebook-f"></i> Facebook</a>
                <a href="javascript:void(0)" class="wave-effect-light btn btn-raised btn-twitter"><i class="fab fa-twitter"></i> Twitter</a>
                <a href="javascript:void(0)" class="wave-effect-light btn btn-raised btn-google"><i class="fab fa-google"></i> Google</a>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="ms-register-tab">
              <form>
                <fieldset>
                  <div class="form-group label-floating">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-user"></i></span>
                      <label class="control-label" for="ms-form-user-r">Username</label>
                      <input type="text" id="ms-form-user-r" class="form-control">
                    </div>
                  </div>
                  <div class="form-group label-floating">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-envelope"></i></span>
                      <label class="control-label" for="ms-form-email-r">Email</label>
                      <input type="email" id="ms-form-email-r" class="form-control">
                    </div>
                  </div>
                  <div class="form-group label-floating">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-lock"></i></span>
                      <label class="control-label" for="ms-form-pass-r">Password</label>
                      <input type="password" id="ms-form-pass-r" class="form-control">
                    </div>
                  </div>
                  <div class="form-group label-floating">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-lock"></i></span>
                      <label class="control-label" for="ms-form-pass-rn">Re-type Password</label>
                      <input type="password" id="ms-form-pass-rn" class="form-control">
                    </div>
                  </div>
                  <button class="btn btn-raised btn-block btn-primary">Register Now</button>
                </fieldset>
              </form>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="ms-recovery-tab">
              <fieldset>
                <div class="form-group label-floating">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fas fa-user"></i></span>
                    <label class="control-label" for="ms-form-user-re">Username</label>
                    <input type="text" id="ms-form-user-re" class="form-control">
                  </div>
                </div>
                <div class="form-group label-floating">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fas fa-envelope"></i></span>
                    <label class="control-label" for="ms-form-email-re">Email</label>
                    <input type="email" id="ms-form-email-re" class="form-control">
                  </div>
                </div>
                <button class="btn btn-raised btn-block btn-primary">Send Password</button>
              </fieldset>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <header class="ms-header ms-header-primary">
    <!--ms-header-primary-->
    <div class="container container-full">
      <div class="ms-title">
        <a href="{{Route('front.index')}}">
          <!-- <img src="{{asset("frontend/assets/img/logo-header.png")}}")}}" alt=""> -->
          <img src="{{asset('backend/uploads/image/'.$logo_data->limage)}}" style="width:50px; height:50px; border-radius:100px!important;"  >
          <!-- <h1 class="animated fadeInRight animation-delay-6">EE <span>E</span></h1> -->
          <h1 class="animated fadeInRight animation-delay-6">{{$logo_data->name}}</h1>

          
        </a>
      </div>
      <div class="header-right">
        <div class="share-menu">
          <ul class="share-menu-list">
            <li class="animated fadeInRight animation-delay-3"><a href="javascript:void(0)" class="btn-circle btn-google"><i class="fab fa-instagram"></i></a></li>
            <li class="animated fadeInRight animation-delay-2"><a href="javascript:void(0)" class="btn-circle btn-facebook"><i class="fab fa-facebook-f"></i></a></li>
            <li class="animated fadeInRight animation-delay-1"><a href="javascript:void(0)" class="btn-circle btn-twitter"><i class="fab fa-twitter"></i></a></li>
          </ul>
          <a href="javascript:void(0)" class="btn-circle btn-circle-primary animated zoomInDown animation-delay-7"><i class="fas fa-share-alt"></i></a>
        </div>
        <a href="javascript:void(0)" class="btn-circle btn-circle-primary no-focus animated zoomInDown animation-delay-8" data-toggle="modal" data-target="#ms-account-modal"><i class="fas fa-user"></i></a>
        <form class="search-form animated zoomInDown animation-delay-9" action="/search" method="POST" role="search">
          {{ csrf_field() }}
          <div class="input-group">
            {{-- <input type="text" class="form-control" name="q"
                placeholder="Search users"> <span class="input-group-btn"> --}}
          <input id="search-box" type="text" class="search-input" placeholder="Search..." name="q" />

                <button type="submit" class="btn btn-default">
                    {{-- <span class="glyphicon glyphicon-search"></span> --}}
                    <i class="fas fa-search"></i>
                </button>
            </span>
        </div>

          {{-- <input id="search-box" type="text" class="search-input" placeholder="Search..." name="q" />
          <label for="search-box"><i class="fas fa-search"></i></label> --}}
        </form>
        {{-- <form action="/search" method="POST" role="search">
          {{ csrf_field() }}
          <div class="input-group">
              <input type="text" class="form-control" name="q"
                  placeholder="Search users"> <span class="input-group-btn">
                  <button type="submit" class="btn btn-default">
                      <span class="glyphicon glyphicon-search"></span>
                  </button>
              </span>
          </div>
      </form> --}}
        <a href="javascript:void(0)" class="btn-ms-menu btn-circle btn-circle-primary ms-toggle-left animated zoomInDown animation-delay-10"><i class="fas fa-bars"></i></a>
      </div>
    </div>
  </header>
  <nav class="navbar navbar-expand-md  navbar-static ms-navbar ms-navbar-primary">
    <div class="container container-full">
      <div class="navbar-header">
        <a class="navbar-brand" href="{{Route('front.index')}}">
          <!-- <img src="{{asset("frontend/assets/img/logo-navbar.png")}}")}}" alt=""> -->
          <img src="{{asset('backend/uploads/image/'.$logo_data->limage)}}" style="width:50px; height:50px; border-radius:100px!important;"  >
          <span class="ms-title">{{$logo_data->name}}</span>
          
        </a>
      </div>
      <div class="collapse navbar-collapse" id="ms-navbar">
        <ul class="navbar-nav">
          <li class="{{Request::is('/')?'nav-item active':'nav-item'}}">
            <a href="/" class="animated fadeIn animation-delay-7" >Home</a>
          </li>

          <li class="{{Request::is('about')?'nav-item active':'nav-item'}}">
            <a href="/about" class="animated fadeIn animation-delay-7" >About Us</a>
          </li>

          <li class="{{Request::is('service')?'nav-item active':'nav-item'}}">
            <a href="/service" class="nav-link animated fadeIn animation-delay-7">Services</a>
          </li>
          
          <li class="{{Request::is('portfolio')?'nav-item active':'nav-item'}}">
            <a href="/portfolio" class="animated fadeIn animation-delay-7" >Portfolio</a>
          </li>
       
          <!-- <li class="{{Request::is('ourteam')?'nav-item active':'nav-item'}}">
            <a href="/ourteam" class="animated fadeIn animation-delay-7" >Our Team</a>
          </li> -->

          <li class="{{Request::is('contact')?'nav-item active':'nav-item'}}">
            <a href="/contact" class="animated fadeIn animation-delay-7" >Contact</a>
          </li>

          <li class="{{Request::is('blog')?'nav-item active':'nav-item'}}">
            <a href="/blog" class="animated fadeIn animation-delay-7" >Blog</a>
          </li>
          
        </ul>
      </div>
      <a href="javascript:void(0)" class="ms-toggle-left btn-navbar-menu"><i class="fas fa-bars eee-toggle-left"></i></a>
    </div> <!-- container -->
  </nav>
 
  </div> <!-- ms-hero ms-hero-black -->