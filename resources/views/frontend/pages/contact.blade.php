@extends('frontend.main')
@section('title','| Contact')
@section('content')
 
      <!-- Modal -->
      
     
      <div class="container">
        <div class="row">
          <div class="col-xl-8 col-lg-7">
            <div class="card card-primary animated fadeInUp animation-delay-7">
              <div class="ms-hero-bg-primary ms-hero-img-mountain">
                <h2 class="text-center no-m pt-4 pb-4 color-white index-1">Contact</h2>
              </div>
              <div class="card-body">
                  {{Form::open(['route'=>'saveform','method'=>'POST','data-parsely-validate'=>'','files'=>'true'])}}

                    <div class="form-group row">
                      <label for="inputEmail" autocomplete="false" class="col-lg-2 control-label">Name</label>
                      <div class="col-lg-9">
                        <input type="text" class="form-control" name="name" id="inputName" placeholder="Name">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail" autocomplete="false" class="col-lg-2 control-label">Email</label>
                      <div class="col-lg-9">
                        <input type="email" class="form-control" name="email" id="inputEmail" placeholder="Email">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputEmail" autocomplete="false" class="col-lg-2 control-label">Subject</label>
                      <div class="col-lg-9">
                        <input type="text" class="form-control" name="subject" id="inputSubject" placeholder="Subject">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="textArea" class="col-lg-2 control-label">Message</label>
                      <div class="col-lg-9">
                        <textarea class="form-control" rows="3" name="msg" id="textArea" placeholder="Your message..."></textarea>
                      </div>
                    </div>
                    <div class="form-group row justify-content-end">
                      <div class="col-lg-10">
                        {{-- <button type="submit" class="btn btn-raised btn-primary">Send Message</button> --}}
                        {{Form::submit('Send message',["class"=>"btn btn-raised btn-primary"])}}
                        <!-- <button type="button" class="btn btn-danger">Cancel</button> -->
                      </div>
                    </div>
                  </fieldset>
                {{Form::close()}}
              </div>
            </div>
          </div>
          
          <div class="col-xl-4 col-lg-5">
            <div class="card card-primary animated fadeInUp animation-delay-7">
              <div class="card-body">
                <div class="text-center mb-2">
                <a href="{{Route('front.index')}}">
                <img src="{{asset('backend/uploads/image/'.$logo_data->limage)}}" style="width:50px; height:50px; border-radius:100px!important;"  >
                  <h3 class="no-m ms-site-title">{{$logo_data->name}}</span></h3></a>
                </div>
                @foreach($contact as $contact)
                <address class="no-mb">
                  <p><i class="color-danger-light fas fa-map-marker-alt"></i> {{$contact->address1}}</p>
                  <p><i class="color-warning-light fas fa-map"></i> {{$contact->address2}}</p>
                  <p><i class="color-info-light fas fa-envelope"></i> <a href="mailto:{{$contact->email}}">{{$contact->email}}</a></p>
                  <p><i class="color-royal-light fas fa-phone"></i>  {{$contact->contact1}} </p>
                  <p><i class="color-success-light fas fa-fax"></i>  {{$contact->contact2}} </p>
                </address>
                @endforeach
              </div>
            </div>

            <div class="card card-primary animated fadeInUp animation-delay-7">
              <div class="card-header">
                <h3 class="card-title"><i class="fas fa-map"></i> Map</h3>
              </div>
              
                <iframe class="innovation-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8403.713996258986!2d85.34170511736363!3d27.676302360720207!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19f2804a02bf%3A0x85468199859b2d8d!2sKoteshwor%2C+Kathmandu+44600!5e0!3m2!1sen!2snp!4v1559320248366!5m2!1sen!2snp" width="350" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                
            </div>
            
          </div>
        </div>
      </div> <!-- container -->
      @endsection
  