@extends('frontend.main')
@section('title','| Portfolio')
@section('content')

    <a href="javascript:void(0)" class="ms-conf-btn ms-configurator-btn btn-circle btn-circle-raised btn-circle-primary animated rubberBand"><i class="fa fa-gears"></i></a>
    <div id="ms-configurator" class="ms-configurator">
      <div class="ms-configurator-title">
        <h3><i class="fa fa-gear"></i> Theme Configurator</h3>
        <a href="javascript:void(0);" class="ms-conf-btn withripple"><i class="zmdi zmdi-close"></i></a>
      </div>
      <div class="panel-group" id="accordion_conf" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="ms-conf-header-color">
            <h4 class="panel-title">
              <a role="button" class="withripple" data-toggle="collapse" href="#ms-collapse-conf-1" aria-expanded="true" aria-controls="ms-collapse-conf-1">
                <i class="zmdi zmdi-invert-colors"></i> Color Selector </a>
            </h4>
          </div>
          <div id="ms-collapse-conf-1" class="card-collapse collapse show" role="tabpanel" aria-labelledby="ms-conf-header-color" data-parent="#accordion_conf">
            <div class="panel-body">
              <div id="color-options" class="ms-colors-container">
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary red" data-color="red">red</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary pink" data-color="pink">pink</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary purple" data-color="purple">purple</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary deep-purple" data-color="deep-purple">deep-purple</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary indigo" data-color="indigo">indigo</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary blue" data-color="blue">blue</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary light-blue active" data-color="light-blue">light-blue</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary cyan" data-color="cyan">cyan</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary teal" data-color="teal">teal</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary green" data-color="green">green</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary light-green" data-color="light-green">light-green</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary lime" data-color="lime">lime</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary yellow" data-color="yellow">yellow</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary amber" data-color="amber">amber</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary orange" data-color="orange">orange</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary deep-orange" data-color="deep-orange">deep-orange</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary brown" data-color="brown">brown</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary grey" data-color="grey">grey</a>
                <a href="javascript:void(0);" class="ms-color-box ms-color-box-primary blue-grey" data-color="blue-grey">blue-grey</a>
              </div>
              <div id="grad-options" class="ms-color-shine">
                <h4 class="no-mb text-center">Color Brightness</h4>
                <span>300</span><span>400</span><span>500</span><span>600</span><span>700</span><span>800</span>
                <a href="javascript:void(0)" data-shine=300 class="ms-color-box grad c300 light-blue">300</a>
                <a href="javascript:void(0)" data-shine=400 class="ms-color-box grad c400 light-blue">400</a>
                <a href="javascript:void(0)" data-shine=500 class="ms-color-box grad c500 light-blue active">500</a>
                <a href="javascript:void(0)" data-shine=600 class="ms-color-box grad c600 light-blue">600</a>
                <a href="javascript:void(0)" data-shine=700 class="ms-color-box grad c700 light-blue">700</a>
                <a href="javascript:void(0)" data-shine=800 class="ms-color-box grad c800 light-blue">800</a>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="ms-conf-header-headers">
            <h4 class="panel-title">
              <a class="collapsed withripple" role="button" data-toggle="collapse" href="#ms-collapse-conf-2" aria-expanded="false" aria-controls="ms-collapse-conf-2">
                <i class="zmdi zmdi-view-compact"></i> Header Styles </a>
            </h4>
          </div>
          <div id="ms-collapse-conf-2" class="card-collapse collapse" role="tabpanel" aria-labelledby="ms-conf-header-headers" data-parent="#accordion_conf">
            <div class="panel-body">
              <!--<h5>Preset Options</h5>
                    <form class="form-inverse ms-conf-radio">
                        <div class="form-group">
                            <div class="radio radio-primary">
                                <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">Default Style
                                </label>
                            </div>
                            <div class="radio radio-primary">
                                <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">Pure Material
                                </label>
                            </div>
                            <div class="radio radio-primary">
                                <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">Navbar Mode
                                </label>
                            </div>
                        </div>
                    </form>
                    <h5>Custom Header</h5>-->
              <h6>Header Options</h6>
              <form class="form-inverse ms-conf-radio" id="header-config">
                <div class="form-group">
                  <div class="radio radio-primary">
                    <label>
                      <input type="radio" name="customHeader" id="whiteHeader" value="white" checked="cheked"> Light Color </label>
                  </div>
                  <div class="radio radio-primary">
                    <label>
                      <input type="radio" name="customHeader" id="primaryHeader" value="primary"> Primary Color </label>
                  </div>
                  <div class="radio radio-primary">
                    <label>
                      <input type="radio" name="customHeader" id="darkHeader" value="dark"> Dark Color </label>
                  </div>
                  <div class="radio radio-primary">
                    <label>
                      <input type="radio" name="customHeader" id="noHeader" value="hidden"> No Header (Navbar Mode) </label>
                  </div>
                </div>
              </form>
              <h6>Navbar Options</h6>
              <form class="form-inverse ms-conf-radio" id="navbar-config">
                <div class="form-group">
                  <div class="radio radio-primary">
                    <label>
                      <input type="radio" name="customNavbar" id="whiteNavbar" value="white" checked=""> Light Color </label>
                  </div>
                  <div class="radio radio-primary">
                    <label>
                      <input type="radio" name="customNavbar" id="primaryNavbar" value="primary"> Primary Color </label>
                  </div>
                  <div class="radio radio-primary">
                    <label>
                      <input type="radio" name="customNavbar" id="darkNavbar" value="dark"> Dark Color </label>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="ms-conf-header-container">
            <h4 class="panel-title">
              <a class="collapsed withripple" role="button" data-toggle="collapse" href="#ms-conf-collapse-3" aria-expanded="false" aria-controls="ms-conf-collapse-3">
                <i class="zmdi zmdi-grid"></i> Container Options </a>
            </h4>
          </div>
          <div id="ms-conf-collapse-3" class="card-collapse collapse" role="tabpanel" aria-labelledby="ms-conf-header-container" data-parent="#accordion_conf">
            <div class="panel-body">
              <form class="form-inverse ms-conf-radio" id="boxed-config">
                <div class="form-group">
                  <div class="radio radio-primary">
                    <label>
                      <input type="radio" name="customWidth" id="fullWidth" value="full" checked=""> Full Width </label>
                  </div>
                  <div class="radio radio-primary">
                    <label>
                      <input type="radio" name="customWidth" id="boxedWidth" value="boxed"> Boxed Mode </label>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
   
    <div class="ms-site-container">
      <!-- Modal -->
     
    
      <div class="material-background"></div>
      <div class="container">
        <div class="text-center mb-6">
            @foreach ($portfoliotitle as $portfoliotitle)
                
            <h1 class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5">{{$portfoliotitle->title}}</h1>
          <p class="lead lead-lg color-medium text-center center-block mt-2 mw-800 text-uppercase fw-300 animated fadeInUp animation-delay-7"><span class="colorStar">
          </span>  <span class="colorStar">
            {{-- {{strip_tags($portfoliotitle->detail)}} --}}
            {!!$portfoliotitle->detail!!}

            {{-- {{ nl2br(e($portfoliotitle->detail))}} --}}
            {{-- {{$portfoliotitle->detail}} --}}
          </span></p>

            @endforeach
        </div>
        <div class="card d-none d-md-block">
          <div class="card-header bg-primary">
            <h3 class="card-title text-center"><i class="zmdi zmdi-filter-list"></i>Filter List</h3>
          </div>
          <div class="row no-m">
            <div class="col-lg-8 col-sm-12 no-pr no-pl">
              <h4 class="text-center no-mb">Categories</h4>
              <ul class="nav nav-tabs nav-tabs-transparent indicator-primary nav-tabs-full nav-tabs-7" role="tablist">
                <li class="nav-item"><a class="nav-link withoutripple no-pl no-pr filter active" data-filter="all" href="#cat" aria-controls="cat" role="tab" data-toggle="tab">All</a></li>
                <li class="nav-item"><a class="nav-link withoutripple no-pl no-pr filter" data-filter=".category-1" href="#cat1" aria-controls="cat1" role="tab" data-toggle="tab">Web Design</span></a></li>
                <li class="nav-item"><a class="nav-link withoutripple no-pl no-pr filter" data-filter=".category-2" href="#cat2" aria-controls="cat2" role="tab" data-toggle="tab">Nature</a></li>
                <li class="nav-item"><a class="nav-link withoutripple no-pl no-pr filter" data-filter=".category-3" href="#cat3" aria-controls="cat3" role="tab" data-toggle="tab">People</a></li>
                <li class="nav-item"><a class="nav-link withoutripple no-pl no-pr filter" data-filter=".category-4" href="#cat4" aria-controls="cat4" role="tab" data-toggle="tab">Landscape</a></li>
                <li class="nav-item"><a class="nav-link withoutripple no-pl no-pr filter" data-filter=".category-5" href="#cat5" aria-controls="cat5" role="tab" data-toggle="tab">Cities</a></li>
                <li class="nav-item"><a class="nav-link withoutripple no-pl no-pr filter" data-filter=".category-6" href="#cat6" aria-controls="cat6" role="tab" data-toggle="tab">Fashion</a></li>
              </ul>
            </div>
            <div class="col-lg-2 col-sm-6 no-pr no-pl">
              <h4 class="text-center no-mb">Columns</h4>
              <ul class="nav nav-tabs nav-tabs-transparent indicator-primary nav-tabs-full nav-tabs-4" role="tablist">
                <li class="nav-item"><a id="Cols1" class="nav-link withoutripple" href="#home7" aria-controls="home7" role="tab" data-toggle="tab">1</a></li>
                <li class="nav-item"><a id="Cols2" class="nav-link withoutripple" href="#profile7" aria-controls="profile7" role="tab" data-toggle="tab">2</span></a></li>
                <li class="nav-item"><a id="Cols3" class="nav-link withoutripple active" href="#messages7" aria-controls="messages7" role="tab" data-toggle="tab">3</a></li>
                <li class="nav-item"><a id="Cols4" class="nav-link withoutripple" href="#settings7" aria-controls="settings7" role="tab" data-toggle="tab">4</a></li>
              </ul>
            </div>
            <div class="col-lg-2 col-sm-6 no-pr no-pl">
              <h4 class="text-center no-mb">Description</h4>
              <ul class="nav nav-tabs nav-tabs-transparent indicator-primary nav-tabs-full nav-tabs-2" role="tablist">
                <li class="nav-item"><a id="portShow" class="nav-link withoutripple" href="#show" aria-controls="show" role="tab" data-toggle="tab">Show</a></li>
                <li class="nav-item"><a id="portHide" class="nav-link withoutripple active" href="#hide" aria-controls="hide" role="tab" data-toggle="tab">Hide</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="row text-center" id="Container">
              <div class="col-md-4 col-sm-6 mix category-1">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port1.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-2">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port2.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-3">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port3.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-1">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port4.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-4">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port5.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-6 category-3">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port6.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-2 category-3">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port7.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-1">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port8.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-5">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port9.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-2">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port10.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-4">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port11.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-6">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port12.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-1">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port13.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-2">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port14.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-3">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port15.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-1">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port16.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-4">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port17.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-6">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port18.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-2">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port19.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-1">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port20.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-5">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port21.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-2">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port22.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-4">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port23.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
              <div class="col-md-4 col-sm-6 mix category-6">
                <div class="card width-auto">
                  <figure class="ms-thumbnail">
                    <img src="assets/img/demo/port24.jpg" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                      <div class="ms-thumbnail-caption-content">
                        <h4 class="ms-thumbnail-caption-title mb-2">Lorem ipsum dolor</h4>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mr-1 btn-circle-white color-danger"><i class="zmdi zmdi-favorite"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 mr-1 btn-circle-white color-warning"><i class="zmdi zmdi-star"></i></a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs ml-1 btn-circle-white color-success"><i class="zmdi zmdi-share"></i></a>
                      </div>
                    </figcaption>
                  </figure>
                  <div class="card-body overflow-hidden text-center portfolio-item-caption d-none">
                    <h3 class="color-primary no-mt">Lorem ipsum dolor</h3>
                    <p>Explicabo consequatur quidem praesentium quas qui eius ina Cupiditate ratione sint.</p>
                  </div>
                </div>
              </div> <!-- item -->
            </div>
          </div>
        </div>
      </div> <!-- container -->
     @endsection