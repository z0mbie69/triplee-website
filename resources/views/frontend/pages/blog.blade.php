@extends('frontend.main')
@section('title','| Blog')

@section('content')
      <!-- Modal -->
      
      
     
    
     <div class="ms-hero-page ms-hero-img-keyboard ms-hero-bg-primary mb-6">
        <div class="container">

        </div>
      </div>

      <div class="container">
        <div class="row">
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.4&appId=241110544128";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-share-button" data-href="http://YourPageLink.com" data-layout="button_count"></div>
          
          <div class="col-lg-8">
          @foreach($blogs as $blog)
            <article class="card mb-4 wow materialUp animation-delay-5" style="visibility: visible; animation-name: materialUp;">
              <figure class="ms-thumbnail ms-thumbnail-diagonal">
                <img src="{{asset('backend/uploads/image/'.$blog->bimage)}}"  alt="" class="img-fluid">
                <figcaption class="ms-thumbnail-caption text-center">
                  <div class="ms-thumbnail-caption-content">
                    <h3 class="ms-thumbnail-caption-title">{!!$blog->image_title!!}</h3>
                    {!!$blog->image_description!!}
                    <div class="mt-2">
                      <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm mr-1 btn-circle-white color-danger"><i class="fab fa-facebook-f"></i></a>
                      <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 mr-1 btn-circle-white color-warning"><i class="fab fa-twitter"></i></a>
                      <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm ml-1 btn-circle-white color-success"><i class="fab fa-instagram"></i></a>
                    </div>
                  </div>
                </figcaption>
              </figure>

              <!-- blog -->
              <div class="card-body">
                <h2><a href="javascript:void(0)">{!!$blog->title!!}</a></h2>
                {{substr(strip_tags($blog->description),0,30)}}{{strlen($blog->description)>15?"...":""}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="mt-1">
                      @foreach($blog->category as $category)
                      <a href="#" class="ms-tag ms-tag-info">{{$category}}</a>
                      @endforeach
                     
                    </div>
                  </div>
                  <div class="col-md-6">
                    <a href="{{Route('fblog.show',$blog->id)}}" class="btn btn-primary btn-raised btn-block animate-icon">Read more <i class="fas fa-long-arrow-alt-right"></i></a>
                  </div>
                </div>
              </div>
            </article>
            @endforeach
            <div class="center">
              {{$blogs->links()}}
            </div>
            {{-- <nav aria-label="Page navigation">
              <ul class="pagination">
                <li class="page-item">
                  <a class="page-link" href="javascript:void(0)" aria-label="Previous">
                    <span aria-hidden="true">«</span>
                  </a>
                </li>
                <li class="page-item active"><a class="page-link" href="javascript:void(0)">1</a></li>
                <li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                <li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                <li class="page-item"><a class="page-link" href="javascript:void(0)">4</a></li>
                <li class="page-item">
                  <a class="page-link" href="javascript:void(0)" aria-label="Next">
                    <span aria-hidden="true">»</span>
                  </a>
                </li>
              </ul>
            </nav> --}}
          </div>
          <div class="col-lg-4">
            <!-- <div class="card animated fadeInUp animation-delay-7">
              <div class="ms-hero-bg-royal ms-hero-img-coffee">
                <h3 class="color-white index-1 text-center no-m pt-4">Victoria Smith</h3>
                <div class="color-medium index-1 text-center np-m">@vic_smith</div>
                <img src="assets/img/avatar1.jpg" alt="..." class="img-avatar-circle">
              </div>
              <div class="card-body pt-4 text-center">
                <h3 class="color-primary">About me</h3>
                <p>Lorem ipsum dolor sit amet, consectetur alter adipisicing elit. Facilis, natuse inse voluptates officia repudiandae beatae magni es magnam autem molestias.</p>
                <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-facebook"><i class="fab fa-facebook-f"></i></a>
                <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-twitter"><i class="fab fa-twitter"></i></a>
                <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-instagram"><i class="fab fa-instagram"></i></a>
              </div>
            </div> -->
            <div class="card animated fadeInUp animation-delay-7">
              <div class="card-tabs">
                <ul class="nav nav-tabs nav-tabs-full nav-tabs-4 shadow-2dp" role="tablist">
                  <li class="nav-item current"><a href="#favorite" aria-controls="favorite" role="tab" data-toggle="tab" class="nav-link withoutripple active" aria-selected="true"><i class="fas fa-star"></i></a></li>
                  <!-- <li class="nav-item"><a href="#categories" aria-controls="categories" role="tab" data-toggle="tab" class="nav-link withoutripple" aria-selected="false"><i class="fas fa-folder"></i></a></li>
                  <li class="nav-item"><a href="#archives" aria-controls="archives" role="tab" data-toggle="tab" class="nav-link withoutripple" aria-selected="false"><i class="fas fa-clock"></i></a></li>
                  <li class="nav-item"><a href="#tags" aria-controls="tags" role="tab" data-toggle="tab" class="nav-link withoutripple" aria-selected="false"><i class="fas fa-tags"></i></a></li> -->
                <span id="i3" class="ms-tabs-indicator" style="left: 0px; width: 87.5px;"></span></ul>
              </div>
              
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade active show" id="favorite">
                  <div class="card-body">
                  @foreach($blog2 as $blog2)
                    <div class="ms-media-list">
                      <div class="media mb-2">
                      
                        <div class="media-left media-middle">
                          <a href="{{route('fblog.show',$blog2->id)}}">
                            <img class="d-flex mr-3 media-object media-object-circle" src="{{asset('backend/uploads/image/'.$blog2->bimage)}}"  alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          
                          <a href="{{route('fblog.show',$blog2->id)}}" class="media-heading">{!!$blog2->title!!}</a>
                          <div class="media-footer text-small">
                            <span class="mr-1"><i class="fas fa-clock color-primary"></i> {{date('M-j-Y ',strtotime($blog2->created_at))}}</span>
                           @foreach($blog->category as $category)
                            <span><i class="far fa-folder color-success"></i> <a href="#">{{$category}}</a></span>
                            @endforeach
                          </div>
                         
                        </div>
                      </div>

                    </div>
                     @endforeach
                  </div>
                </div>
                
              </div>
            </div>
            
          </div>
        </div>
      </div>
         
@endsection