@extends('frontend.main')
@section('title','| service')
@section('content')
      <!-- Modal -->
   
    
      <header class="ms-hero-page ms-hero-bg-dark-light ms-hero-img-city2 mb-4 ms-bg-fixed">
        <div class="container">
          <div class="text-center">
            @foreach ($servicetitle as $servicetitle)

            <h1 class="color-info mb-4 animated fadeInDown animation-delay-4">{{$servicetitle->title}}</h1>
          <p class="lead lead-xl mw-800 center-block color-medium mb-2 animated fadeInDown animation-delay-4"><span class="colorStar"><span class="colorStar animation-delay-10">
            {{-- {!! nl2br(e($servicetitle->detail)) !!} --}}
          {!! $servicetitle->detail !!}
          </span>.</span> </p>

            @endforeach
            
          </div>
          <div class="row mt-4">
            @foreach ($service as $service)
            <div class="col-lg-4 col-sm-6 mb-2">
                <div class="ms-icon-feature wow flipInX animation-delay-4" style="visibility: visible; animation-name: flipInX;">
                  <div class="ms-icon-feature-icon">
                    <span class="ms-icon ms-icon-lg ms-icon-inverse ms-icon-white"><i class="fa fa-{{$service->icon}}"></i></span>
                  </div>
                  <div class="ms-icon-feature-content color-white">
                    <h4 class="color-info">{{substr(strip_tags($service->title),0,22)}}{{strlen($service->title)>15?"...":""}}</h4>
                    <p> {{substr(strip_tags($service->description),0,22)}}{{strlen($service->description)>15?"...":""}}</p>
                  </div>
                </div>
              </div>
            @endforeach
      
            {{-- <div class="col-lg-4 col-sm-6 mb-2">
              <div class="ms-icon-feature wow flipInX animation-delay-4" style="visibility: visible; animation-name: flipInX;">
                <div class="ms-icon-feature-icon">
                  <span class="ms-icon ms-icon-lg ms-icon-inverse ms-icon-white"><i class="fa fa-desktop"></i></span>
                </div>
                <div class="ms-icon-feature-content color-white">
                  <h4 class="color-info">Web Design and SEO</h4>
                  <p>Praesentium cumque voluptate harum quae doloribus, atque error debitis, amet velit in similique, necessitatibus odit vero sunt.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 mb-2">
              <div class="ms-icon-feature wow flipInX animation-delay-4" style="visibility: visible; animation-name: flipInX;">
                <div class="ms-icon-feature-icon">
                  <span class="ms-icon ms-icon-lg ms-icon-inverse ms-icon-white"><i class="fa fa-tablet"></i></span>
                </div>
                <div class="ms-icon-feature-content color-white">
                  <h4 class="color-info">Mobile and Tablet Apps</h4>
                  <p>Praesentium cumque voluptate harum quae doloribus, atque error debitis, amet velit in similique, necessitatibus odit vero sunt.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 mb-2">
              <div class="ms-icon-feature wow flipInX animation-delay-4" style="visibility: visible; animation-name: flipInX;">
                <div class="ms-icon-feature-icon">
                  <span class="ms-icon ms-icon-lg ms-icon-inverse ms-icon-white"><i class="fa fa-wordpress"></i></span>
                </div>
                <div class="ms-icon-feature-content color-white">
                  <h4 class="color-info">Wordpress Themes</h4>
                  <p>Praesentium cumque voluptate harum quae doloribus, atque error debitis, amet velit in similique, necessitatibus odit vero sunt.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 mb-2">
              <div class="ms-icon-feature wow flipInX animation-delay-4" style="visibility: visible; animation-name: flipInX;">
                <div class="ms-icon-feature-icon">
                  <span class="ms-icon ms-icon-lg ms-icon-inverse ms-icon-white"><i class="fa fa-graduation-cap"></i></span>
                </div>
                <div class="ms-icon-feature-content color-white">
                  <h4 class="color-info">Training and development</h4>
                  <p>Praesentium cumque voluptate harum quae doloribus, atque error debitis, amet velit in similique, necessitatibus odit vero sunt.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-sm-6 mb-2">
              <div class="ms-icon-feature wow flipInX animation-delay-4" style="visibility: visible; animation-name: flipInX;">
                <div class="ms-icon-feature-icon">
                  <span class="ms-icon ms-icon-lg ms-icon-inverse ms-icon-white"><i class="fa fa-send"></i></span>
                </div>
                <div class="ms-icon-feature-content color-white">
                  <h4 class="color-info">Customer service</h4>
                  <p>Praesentium cumque voluptate harum quae doloribus, atque error debitis, amet velit in similique, necessitatibus odit vero sunt.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="text-center">
            <a href="javascript:void(0)" class="btn btn-raised btn-white color-royal animated fadeInUp animation-delay-10"><i class="zmdi zmdi-desktop-mac"></i> Portfolio</a>
            <a href="javascript:void(0)" class="btn btn-raised btn-white color-success animated fadeInUp animation-delay-10"><i class="zmdi zmdi-email"></i> Concact us</a>
          </div> --}}
        </div>
      </header>
     
      <!-- <div class="container">
        <h1 class="right-line">Our Values</h1>
        <div class="row">
          <div class="col-md-6">
            <ol class="service-list list-unstyled">
              @foreach ($value as $value)
              <li>{!!$value->detail!!}.</li>
                  
              @endforeach
              {{-- <li>Totam porro sit, obcaecati quos quae iure tenetur, soluta voluptatem sapiente rerum ipsam delectus corporis voluptates voluptate, nulla mollitia pariatur.</li>
              <li>Amet dolorum ullam, rerum ratione distinctio, quia iusto rem! Asperiores et quas, ratione in dolores dolorum doloribus magni suscipit labore!</li>
              <li>Enim quas nesciunt sequi odit, ut <a href="#">quisquam vitae commodi</a> animi placeat nihil saepe magnam aliquam, vero harum quae doloribus aut nostrum veniam alias!</li>
              <li>Expedita doloribus vel nam fuga iusto aperiam maxime aut amet pariatur. Libero quidem, optio itaque ducimus. Nulla laboriosam voluptas voluptates.</li>
              <li>Amet dolorum ullam, rerum ratione distinctio, quia iusto rem! Asperiores et quas, ratione in dolores dolorum doloribus magni suscipit labore!</li>
              <li>Lorem ipsum dolor sit amet, <strong>consectetur adipisicing elit</strong>. Nihil suscipit cupiditate expedita hic earum vero sint, recusandae itaque, rem distinctio.</li> --}}
            </ol>
          </div>
          <div class="col-md-6">
              @foreach ($note as $note)

            <div class="card card-primary mb-4">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-4">
                      <div class="text-center center-block">
                        <img src="{{asset('backend/uploads/image/'.$note->image)}}" alt="" class="img-fluid">
                      </div>
                    </div>
                    <div class="col-lg-8 text-center">
                      <h4 class="color-primary mt-lg-0">{{$note->title}}</h4>
                      <p>{!!$note->detail!!}</p>
                    </div>              
                </div>

              </div>
            </div>
            @endforeach

           
           
          </div>
        </div>
      </div> -->
    
   @endsection