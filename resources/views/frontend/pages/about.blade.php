@extends('frontend.main')
@section('title','| About')

@section('content')

      <!-- Modal -->
      <div class="ms-hero-page-override ms-hero-img-city ms-hero-bg-primary">
        <div class="container">
          <div class="text-center">
          <a href="{{Route('front.index')}}">
          <img src="{{asset('backend/uploads/image/'.$logo_data->limage)}}" style="width:50px; height:50px; border-radius:100px!important;"  ></a>
            @foreach ($abouttitle as $abouttitle)
                
            <h1 class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5">{{$abouttitle->title}}</h1>
          <p class="lead lead-lg color-medium text-center center-block mt-2 mw-800 text-uppercase fw-300 animated fadeInUp animation-delay-7"><span class="colorStar"></span>  <span class="colorStar">
            {{-- {!!nl2br(e($abouttitle->detail))!!}    --}}
            {{-- {{strip_tags($abouttitle->detail)}} --}}
            {!! $abouttitle->detail !!}

            {{-- {{ nl2br(e($abouttitle->detail))}} --}}
            {{-- {{$abouttitle->detail}} --}}
          </span>.</p>

            @endforeach
          </div>
        </div>
      </div>

      <div class="container about-us-content">
        <div class="card card-hero animated slideInUp animation-delay-8 mb-6">
          <div class="card-body">
            <h2 class="color-primary">About Us</h2>
            <div class="row">
                @foreach ($aboutpara1 as $aboutpara1)

              <div class="col-lg-6 text-justify">
                <p class="dropcaps">{!!$aboutpara1->detail!!}</p>

                {{-- <p class="dropcaps">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam ipsa id saepe, quos ullam fugiat velit pariatur alias cumque. Architecto et vitae perferendis cumque ratione officiis? Quas quod, rerum dolores qui. Iste magnam ipsam laborum. Natus quis maiores est qui maxime, consectetur ipsam esse quaerat facilis quos repudiandae eaque magni laboriosam amet.</p>
                <p>Perferendis, blanditiis unde fugiat voluptas molestias velit asperiores rerum ipsam animi eum temporibus at numquam, nobis voluptates minus maxime cum obcaecati! Tenetur sit corporis laudantium inventore officia officiis odio repellat dolore quos repudiandae voluptas ad facere, amet placeat animi voluptatem distinctio beatae.</p>
              </div>
              <div class="col-lg-6 text-justify">
                <p>Non sequi adipisci nostrum natus rem accusamus itaque repellendus illum neque! Voluptate, error commodi a quaerat eveniet tenetur reiciendis nulla doloremque iusto repellat quis asperiores, quibusdam architecto culpa facere aliquam placeat eaque amet, optio nobis alias maiores. Nulla perferendis impedit hic placeat veniam distinctio error.</p>
                <p>Tenetur numquam a, nesciunt neque odit amet, qui quibusdam natus assumenda quas omnis, aspernatur quisquam nobis illum ea distinctio tempora quaerat. Aperiam cumque, eveniet similique praesentium, temporibus, id quis labore aspernatur quod placeat ducimus fuga consequuntur numquam autem. Voluptates repellat.</p> --}}

              </div>
              @endforeach

            </div>
            <hr class="dotted">
            <div class="row">
              {{-- <div class="col-xl-3 col-md-6">
                <div class="card card-info card-flat">
                  <div class="card-body overflow-hidden text-center">
                    <i class="fa fa-rocket color-info font-big"></i>
                    <h4 class="color-info">Lorem ipsum dolor</h4>
                    <p>Id quam odio voluptates porro harum ducimus non provident, dolor, modi accusantium.</p>
                    <a href="javascript:void(0)" class="btn btn-info"><i class="zmdi zmdi-download"></i> Button</a>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-md-6">
                <div class="card card-warning card-flat">
                  <div class="card-body overflow-hidden text-center">
                    <i class="zmdi zmdi-airplane color-warning font-big"></i>
                    <h4 class="color-warning">Lorem ipsum dolor</h4>
                    <p>Id quam odio voluptates porro harum ducimus non provident, dolor, modi accusantium.</p>
                    <a href="javascript:void(0)" class="btn btn-warning"><i class="zmdi zmdi-download"></i> Button</a>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-md-6">
                <div class="card card-danger card-flat">
                  <div class="card-body overflow-hidden text-center">
                    <i class="zmdi zmdi-coffee color-danger font-big"></i>
                    <h4 class="color-danger">Lorem ipsum dolor</h4>
                    <p>Id quam odio voluptates porro harum ducimus non provident, dolor, modi accusantium.</p>
                    <a href="javascript:void(0)" class="btn btn-danger"><i class="zmdi zmdi-download"></i> Button</a>
                  </div>
                </div>
              </div> --}}
              @foreach ($abutton as $abutton)
              <div class="col-xl-3 col-md-6">
                  <div class="card card-success card-flat">
                    <div class="card-body overflow-hidden text-center">
                      <i class="fa fa-{{$abutton->ticon}} fa-2x"></i>
                      <h4 class="color-success">{{$abutton->title}}</h4>
                    <p>
                    {{substr(strip_tags($abutton->detail),0,15)}}
                    {{strlen($abutton->detail)> 15?"...":''}}</p>
                    <a href="javascript:void(0)" class="btn btn-success"><i class="fa fa-2x fa-{{$abutton->bicon}}"></i> Button</a>
                    </div>
                  </div>
                </div>
              @endforeach
             
            </div>
          </div>
        </div>
      </div>

      <div class="container wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
        <div class="row">
          <div class="col-lg-6 text-justify">
            <h2 class="color-primary right-line">Our Vision</h2>
            @foreach ($aboutpara2 as $aboutpara2)
                
            <p class="dropcaps">{!! $aboutpara2->detail !!}</p>
            {{-- <p>Tenetur numquam a, nesciunt neque odit amet, qui quibusdam natus assumenda quas omnis, aspernatur quisquam nobis illum ea distinctio tempora quaerat. Aperiam cumque, eveniet similique praesentium, temporibus, id quis labore aspernatur quod placeat ducimus fuga consequuntur numquam autem. Voluptates repellat.</p>
            <p class="text-right">

              <cite class="lead color primary-color">— Sundar Pichai, CEO Google Inc.</cite>
            </p> --}}
            @endforeach

          </div>
          <div class="col-lg-6">
            <h2 class="color-primary right-line">Our Approach</h2>

            <div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">
                @foreach ($approach as $approach)
                <div class="mb-0 card card-primary wow fadeInUp animation-delay-2" style="visibility: visible; animation-name: fadeInUp;">
                    <div class="card-header" role="tab" id="headingOne2">
                      <h4 class="card-title">
                      <a class="withripple collapsed" role="button" data-toggle="collapse" href="#collapseOne{{$approach->id}}" aria-expanded="false" aria-controls="collapseOne2">
                        <i class="fa fa-{{$approach->icon}}"></i> {{$approach->title}} <div class="ripple-container"></div></a>
                      </h4>
                    </div>
                    <div id="collapseOne{{$approach->id}}" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2" data-parent="#accordion2" style="">
                      <div class="card-body color-dark">
                      <p>{!!$approach->detail!!}</p>
                      </div>
                    </div>
                  </div>
                @endforeach 
              </div>
              
            {{-- <div class="ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">
              <div class="mb-0 card card-primary wow fadeInUp animation-delay-2" style="visibility: visible; animation-name: fadeInUp;">
                <div class="card-header" role="tab" id="headingOne2">
                  <h4 class="card-title">
                    <a class="withripple collapsed" role="button" data-toggle="collapse" href="#collapseOne2" aria-expanded="false" aria-controls="collapseOne2">
                      <i class="fa fa-lightbulb-o"></i> Talent and creativity <div class="ripple-container"></div></a>
                  </h4>
                </div>
                <div id="collapseOne2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingOne2" data-parent="#accordion2" style="">
                  <div class="card-body color-dark">
                    <p>Lorem ipsum dolor sit amet, consectetur <a href="#">adipisicing elit</a>. Obcaecati molestiae id ipsum ipsa repudiandae. Voluptatum quos facilis sequi. Ullam optio eius deleniti dolore quasi doloribus ipsam.</p>
                    <p>Dolores, corrupti, aliquam doloremque accusantium nemo sunt veniam est incidunt perferendis minima obcaecati ex aperiam voluptatibus blanditiis eum suscipit magnam dolorum in adipisci nihil.</p>
                  </div>
                </div>
              </div>

         
              
              <div class="mb-0 card card-primary wow fadeInUp animation-delay-5" style="visibility: visible; animation-name: fadeInUp;">
                <div class="card-header" role="tab" id="headingTwo2">
                  <h4 class="card-title">
                    <a class="withripple collapsed" role="button" data-toggle="collapse" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                      <i class="fa fa-desktop"></i> Design and code <div class="ripple-container"></div></a>
                  </h4>
                </div>
                <div id="collapseTwo2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingTwo2" data-parent="#accordion2" style="">
                  <div class="card-body color-dark">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit dignissimos inventore cupiditate expedita saepe enim nobis nostrum? Laborum, laudantium, maiores, cupiditate, perspiciatis at ad accusamus.</p>
                    <p>Incidunt, harum itaque voluptatum asperiores recusandae explicabo maiores. Alias, quos, ex impedit at error id laborum fugit architecto qui beatae molestiae dolorum rem veritatis quia aliquam totam.</p>
                  </div>
                </div>
              </div>
              <div class="mb-0 card card-primary wow fadeInUp animation-delay-7" style="visibility: visible; animation-name: fadeInUp;">
                <div class="card-header" role="tab" id="headingThree3">
                  <h4 class="card-title">
                    <a class="withripple collapsed" role="button" data-toggle="collapse" href="#collapseThree2" aria-expanded="false" aria-controls="collapseThree2">
                      <i class="fa fa-user"></i> Quality and Support <div class="ripple-container"></div></a>
                  </h4>
                </div>
                <div id="collapseThree2" class="card-collapse collapse" role="tabpanel" aria-labelledby="headingThree2" data-parent="#accordion2" style="">
                  <div class="card-body color-dark">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, rerum unde doloribus accusamus pariatur non expedita quibusdam velit totam obcaecati. Consequatur, deserunt, asperiores quam nisi earum voluptates.</p>
                    <p>Dolorum, aliquam, totam labore saepe error a eum culpa assumenda sint laudantium ipsa iure ullam et dicta nesciunt repellendus optio voluptatibus reprehenderit odit officia fugiat necessitatibus recusandae architecto.</p>
                  </div>
                </div>
              </div>
            </div> --}}
          </div>
        </div>
      </div>

      <div class="container mt-4">
        <h1 class="color-primary text-center mb-4">Some numerical data</h1>
        <div class="row">
          @foreach ($numeric as $numeric)
          <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-primary card-body overflow-hidden text-center wow zoomInUp animation-delay-2" style="visibility: visible; animation-name: zoomInUp;">
                <h2 class="counter">{{$numeric->number}}</h2>
                <i class="fa fa-4x fa-{{$numeric->icon}} primary-color"></i>
                <p class="mt-2 no-mb lead small-caps">{{$numeric->title}}</p>
              </div>
            </div>
          @endforeach
         
          {{-- <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-primary card-body overflow-hidden text-center wow zoomInUp animation-delay-5" style="visibility: visible; animation-name: zoomInUp;">
              <h2 class="counter">64</h2>
              <i class="fa fa-4x fa-briefcase primary-color"></i>
              <p class="mt-2 no-mb lead small-caps">projects done</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-primary card-body overflow-hidden text-center wow zoomInUp animation-delay-4" style="visibility: visible; animation-name: zoomInUp;">
              <h2 class="counter">600</h2>
              <i class="fa fa-4x fa-comments-o primary-color"></i>
              <p class="mt-2 no-mb lead small-caps">comments</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-primary card-body overflow-hidden text-center wow zoomInUp animation-delay-3" style="visibility: visible; animation-name: zoomInUp;">
              <h2 class="counter">3500</h2>
              <i class="fa fa-4x fa-group primary-color"></i>
              <p class="mt-2 no-mb lead small-caps">happy clients</p>
            </div>
          </div> --}}
        </div>
      </div> 

      <!-- <div class="container mt-6">
        <h2 class="right-line no-mt mb-4 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">Our Services</h2>
        <div class="row">
           @foreach ($service as $service)
    
          <div class="col-lg-4 col-md-6 col-sm-6 mb-2">
            <div class="ms-icon-feature wow flipInX animation-delay-4">
              <div class="ms-icon-feature-icon">
              <span class="ms-icon ms-icon-lg ms-icon-inverse"><i class="fa fa-{{$service->icon}} "></i></span>
              </div>
              <div class="ms-icon-feature-content">
                <h4 class="color-primary"> {{$service->title}}</h4>
              <p>{!!$service->description!!}</p>
              </div>
            </div>
          </div>
        @endforeach
        </div>
      </div> -->


      <!-- <section class="mt-4">
        <div class="container">
          <h1 class="color-primary text-center" text-center="">Our Team</h1>
          <div class="row d-flex justify-content-center">
          @foreach($team as $team)

            <div class="col-xl-3 col-lg-6 col-md-6">
              <div class="card mt-4 card-danger wow zoomInUp" style="visibility: visible; animation-name: zoomInUp;">
                <div class="ms-hero-bg-danger ms-hero-img-city">
                <img src="{{asset('backend/uploads/image/'.$team->timage)}}" alt="..." class="img-avatar-circle">
                </div>
                <div class="card-body pt-6 text-center">
                  <h3 class="color-danger">{{$team->name}}</h3>
                  <p>{{$team->description}}</p>
                  <a href="{{$team->facebook_url}}" target="_blank"  class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-facebook"><i class="fab fa-facebook-f"></i></a>
                  <a href="{{$team->twitter_url}}" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-twitter"><i class="fab fa-twitter"></i></a>
                  <a href="{{$team->instagram_url}}" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-instagram"><i class="fab fa-instagram"></i></a>
                </div>
              </div>
            </div>
            @endforeach
           
              </div>
            </div>
          </div>
        </div>
      </section> -->

     @endsection
  