@extends('frontend.main')
@section('title','| OurTeam')
@section('content')
    <div class="ms-site-container">
      <!-- Modal -->
      
    
      <div class="ms-hero-page-override ms-hero-img-team ms-hero-bg-primary">
        <div class="container">
          <div class="text-center">
            @foreach ($teamtitle as $teamtitle)
                
            <h1 class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5">{{$teamtitle->title}}</h1>
          <p class="lead lead-lg color-medium text-center center-block mt-2 mw-800 text-uppercase fw-300 animated fadeInUp animation-delay-7"><span class="colorStar"></span>  <span class="colorStar">{!!$teamtitle->detail!!}</span>.</p>

            @endforeach
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row d-flex justify-content-center card-top">
          @foreach($team as $team)
          <div class="col-lg-4 col-md-6">
            <div class="card card-warning wow zoomInUp mb-4 animation-delay-5" style="visibility: visible; animation-name: zoomInUp;">
              <div class="withripple zoom-img">
                <a href="javascript:void()" class=""><img src="{{asset('backend/uploads/image/'.$team->timage)}}" alt="..." class="img-fluid"></a>
              </div>
              <div class="card-body">
                <span class="badge badge-warning pull-right">{{$team->category}}</span>
                <h3 class="color-warning">{!!$team->name!!}</h3>
                <p>{!!$team->description!!}</p>
                <div class="row">
                  <div class="col">
                    <a href="{{$team->facebook_url}}" target="_blank"  class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-facebook"><i class="fab fa-facebook-f"></i></a>
                    <a href="{{$team->twitter_url}}" target="_blank"  class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-twitter"><i class="fab fa-twitter"></i></a>
                    <a href="{{$team->instagram_url}}" target="_blank" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-instagram"><i class="fab fa-instagram"></i></a>
                  </div>
                  <div class="col text-right">
                    <a href="javascript:void(0)" class="btn btn-raised btn-sm btn-warning"><i class="fas fa-user"></i> Profile</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
@endsection