@extends('frontend.main')
@section('title','| Blog')

@section('content')
      <!-- Modal -->   
     <div class="ms-hero-page ms-hero-img-keyboard ms-hero-bg-primary mb-6">
        <div class="container">

        </div>
      </div>
      <div class="container">
        <div class="row">
        <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <div class="card animated fadeInLeftTiny animation-delay-5">
              <div class="card-body card-body-big">
                <h1 class="no-mt">{{$blog->title}}</h1>
                <div class="mb-4">
                <img src="{{asset('backend/uploads/image/'.$blog->bimage)}}" style="width:50px; height:50px; border-radius:100px!important;"  >

                 @foreach($blog->category as $category)
                      <a href="javascript:void(0)" class="ms-tag ms-tag-info">{{$category}}</a>
                      @endforeach
                     
                  <span class="ml-1 d-none d-sm-inline"><i class="zmdi zmdi-time mr-05 color-info"></i> <span class="color-medium-dark">{{date('M-j-Y ',strtotime($blog->created_at))}}</span></span>
                </div>
                <img src="{{asset('backend/uploads/image/'.$blog->bimage)}}" alt="" class="img-fluid mb-4">
                <p>{!!$blog->description!!}</p>
                      
              </div>
            </div>
          </div>
          <div class="col-lg-4">

          <div class="card animated fadeInUp animation-delay-7">
              <div class="card-tabs">
                <ul class="nav nav-tabs nav-tabs-full nav-tabs-4 shadow-2dp" role="tablist">
                  <li class="nav-item current"><a href="#favorite" aria-controls="favorite" role="tab" data-toggle="tab" class="nav-link withoutripple active" aria-selected="true"><i class="fas fa-star"></i></a></li>
                  <!-- <li class="nav-item"><a href="#categories" aria-controls="categories" role="tab" data-toggle="tab" class="nav-link withoutripple" aria-selected="false"><i class="fas fa-folder"></i></a></li>
                  <li class="nav-item"><a href="#archives" aria-controls="archives" role="tab" data-toggle="tab" class="nav-link withoutripple" aria-selected="false"><i class="fas fa-clock"></i></a></li>
                  <li class="nav-item"><a href="#tags" aria-controls="tags" role="tab" data-toggle="tab" class="nav-link withoutripple" aria-selected="false"><i class="fas fa-tags"></i></a></li> -->
                <span id="i3" class="ms-tabs-indicator" style="left: 0px; width: 87.5px;"></span></ul>
              </div>
              
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade active show" id="favorite">
                  <div class="card-body">
                  @foreach($blog3 as $blog3)
                    <div class="ms-media-list">
                      <div class="media mb-2">
                      
                        <div class="media-left media-middle">
                          <a href="{{route('fblog.show',$blog3->id)}}">
                            <img class="d-flex mr-3 media-object media-object-circle" src="{{asset('backend/uploads/image/'.$blog3->bimage)}}"  alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          
                          <a href="{{route('fblog.show',$blog3->id)}}" class="media-heading">{!!$blog3->title!!}</a>
                          <div class="media-footer text-small">
                            <span class="mr-1"><i class="fas fa-clock color-primary"></i> {{date('M-j-Y ',strtotime($blog3->created_at))}}</span>
                            @foreach($blog->category as $category)
                            <span><i class="far fa-folder color-success"></i> <a href="#">{{$category}}</a></span>
                            @endforeach
                          </div>
                         
                        </div>
                      </div>

                    </div>
                     @endforeach
                  </div>
                </div>
                
              </div>
            </div>
            
            <!-- <div class="card card-primary animated fadeInUp animation-delay-7">
              <div class="card-header">
                <h3 class="card-title"><i class="zmdi zmdi-apps"></i> Navigation</h3>
              </div>
              <div class="card-tabs">
                <ul class="nav nav-tabs nav-tabs-transparent indicator-primary nav-tabs-full nav-tabs-4" role="tablist">
                  <li class="nav-item"><a href="#favorite" aria-controls="favorite" role="tab" data-toggle="tab" class="nav-link withoutripple active"><i class="no-mr zmdi zmdi-star"></i></a></li>
                  
                </ul>
              </div>
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade active show" id="favorite">
                  <div class="card-body">
                    <div class="ms-media-list">
                      <div class="media mb-2">
                        <div class="media-left media-middle">
                          <a href="#">
                            <img class="d-flex mr-3 media-object media-object-circle" src="assets/img/p75.jpg" alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          <a href="javascript:void(0)" class="media-heading">Lorem ipsum dolor sit amet in consectetur adipisicing</a>
                          <div class="media-footer text-small">
                            <span class="mr-1"><i class="zmdi zmdi-time color-info mr-05"></i> August 18, 2016</span>
                            <span><i class="zmdi zmdi-folder-outline color-success mr-05"></i> <a href="javascript:void(0)">Design</a></span>
                          </div>
                        </div>
                      </div>
                      <div class="media mb-2">
                        <div class="media-left media-middle">
                          <a href="#">
                            <img class="d-flex mr-3 media-object media-object-circle" src="assets/img/p75.jpg" alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          <a href="javascript:void(0)" class="media-heading">Nemo enim ipsam voluptatem quia voluptas sit aspernatur</a>
                          <div class="media-footer text-small">
                            <span class="mr-1"><i class="zmdi zmdi-time color-info mr-05"></i> August 18, 2016</span>
                            <span><i class="zmdi zmdi-folder-outline color-danger mr-05"></i> <a href="javascript:void(0)">Productivity</a></span>
                          </div>
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left media-middle">
                          <a href="#">
                            <img class="d-flex mr-3 media-object media-object-circle" src="assets/img/p75.jpg" alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          <a href="javascript:void(0)" class="media-heading">inventore veritatis et vitae quasi architecto beatae </a>
                          <div class="media-footer text-small">
                            <span class="mr-1"><i class="zmdi zmdi-time color-info mr-05"></i> August 18, 2016</span>
                            <span><i class="zmdi zmdi-folder-outline color-royal-light mr-05"></i> <a href="javascript:void(0)">Resources</a></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
              </div> -->
            </div>
            <!--  
            <div class="card card-success animated fadeInUp animation-delay-7">
              <div class="card-header">
                <h3 class="card-title"><i class="zmdi zmdi-play-circle-outline"></i> Feature Video</h3>
              </div>
              <div class="js-player" data-plyr-provider="vimeo" data-plyr-embed-id="94747106"></div>
            </div>
            <div class="card card-primary animated fadeInUp animation-delay-7">
              <div class="card-header">
                <h3 class="card-title"><i class="zmdi zmdi-widgets"></i> Text Widget</h3>
              </div>
              <div class="card-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat ipsam non eaque est architecto doloribus, labore nesciunt laudantium, ex id ea, cum facilis similique tenetur fugit nemo id minima possimus.</p>
              </div>
            </div>-->
          </div>
        </div>
        <!-- <div class="row wow materialUp animation-delay-8">
          <div class="col-md-12">
            <h2 class="color-primary right-line">This is comment</h2>
            <ul class="ms-timeline">
              <li class="ms-timeline-item wow materialUp">
                <div class="ms-timeline-date">
                  <time class="timeline-time" datetime=""><span>Ana Morgan</span></time>
                  <i class="ms-timeline-point bg-primary"></i>
                  <img src="assets/img/avatar6.jpg" class="ms-timeline-point-img">
                </div>
                <div class="card card-primary">
                  <div class="card-body">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur molestias cum quod ab iste libero quasi! Nemo similique nobis voluptate rem sequi dolores, incidunt quia eius totam maiores inventore commodi?</p>
                    <p>Laborum quae similique quo eveniet esse maxime tempora facere facilis, dignissimos, dolor maiores dicta harum. Neque eligendi qui blanditiis eveniet laudantium, alias non voluptates est, minima, nihil modi! Voluptas qui obcaecati dignissimos explicabo unde culpa odit totam animi praesentium laborum? Sit quis eaque optio. Earum.</p>
                  </div>
                </div>
              </li>
  
            </ul>
          </div>
        </div> -->
        
      </div> <!-- container -->

</div>
</div>


         
        
@endsection