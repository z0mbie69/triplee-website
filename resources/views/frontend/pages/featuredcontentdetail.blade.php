@extends('frontend.main')
@section('title','| Featured Content')

@section('content')
      <!-- Modal -->
        
     <div class="ms-hero-page ms-hero-img-keyboard ms-hero-bg-primary mb-6">
        <div class="container">
        </div>
      </div>

      <div class="container">
        <div class="row">
        <div class="container">
        <div class="row">
          <div class="col-lg-8">
          
            <div class="card animated fadeInLeftTiny animation-delay-5">
              <div class="card-body card-body-big">
                <h1 class="no-mt">{{$featuredcontent->label1}}</h1>
                <div class="mb-4">
                <img src="{{asset('backend/uploads/image/'.$featuredcontent->fimage1)}}" style="width:50px; height:50px; border-radius:100px!important;"  >

                
                     
                  <span class="ml-1 d-none d-sm-inline"><i class="zmdi zmdi-time mr-05 color-info"></i> <span class="color-medium-dark">{{date('M-j-Y ',strtotime($featuredcontent->created_at))}}</span></span>
                </div>
                <img src="{{asset('backend/uploads/image/'.$featuredcontent->fimage1)}}" alt="" class="img-fluid mb-4">
                <p>{!!$featuredcontent->description1!!}</p>
                      
              </div>
              
            </div>
          </div>
          <div class="col-lg-4">
          <div class="card animated fadeInUp animation-delay-7">
              <div class="card-tabs">
                <ul class="nav nav-tabs nav-tabs-full nav-tabs-4 shadow-2dp" role="tablist">
                  <li class="nav-item current"><a href="#favorite" aria-controls="favorite" role="tab" data-toggle="tab" class="nav-link withoutripple active" aria-selected="true"><i class="fas fa-star"></i></a></li>
                  <!-- <li class="nav-item"><a href="#categories" aria-controls="categories" role="tab" data-toggle="tab" class="nav-link withoutripple" aria-selected="false"><i class="fas fa-folder"></i></a></li>
                  <li class="nav-item"><a href="#archives" aria-controls="archives" role="tab" data-toggle="tab" class="nav-link withoutripple" aria-selected="false"><i class="fas fa-clock"></i></a></li>
                  <li class="nav-item"><a href="#tags" aria-controls="tags" role="tab" data-toggle="tab" class="nav-link withoutripple" aria-selected="false"><i class="fas fa-tags"></i></a></li> -->
                <span id="i3" class="ms-tabs-indicator" style="left: 0px; width: 87.5px;"></span></ul>
              </div>
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade active show" id="favorite">
                  <div class="card-body">
                  @foreach($featured2 as $featured2)
                    <div class="ms-media-list">
                      <div class="media mb-2">
                      
                        <div class="media-left media-middle">
                          <a href="{{route('feature.show',$featured2->id)}}">
                            <img class="d-flex mr-3 media-object media-object-circle" src="{{asset('backend/uploads/image/'.$featured2->fimage1)}}"  alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          
                          <a href="{{route('feature.show',$featured2->id)}}" class="media-heading">{!!$featured2->label1!!}</a>
                          <div class="media-footer text-small">
                            <span class="mr-1"><i class="fas fa-clock color-primary"></i> {{date('M-j-Y ',strtotime($featured2->created_at))}}</span>
                         
                          </div>
                         
                        </div>
                      </div>

                    </div>
                     @endforeach
                  </div>
                </div>
              </div>
            </div>
            </div>           
          </div>
        </div>       
      </div> <!-- container -->
</div>              
@endsection