<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>EEE Innovation @yield('title')</title>
    <meta name="description" content="EEE Innovation">
    <link rel="stylesheet" href="{{asset('frontend/assets/css/preload.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/assets/css/plugins.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/assets/css/style.light-blue-500.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/assets/css/width-boxed.min.css')}}" id="ms-boxed" disabled="">

	<link rel="icon" href="{{asset('backend/uploads/image/'.$logo_data->limage)}}" style="border-radius: 50% !important;"/>
    <!-- <meta property="og:image" content="C:\xampp\htdocs\tripleee\public\frontend\assets\img\port2.jpg"> -->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">

    <link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/css/custom.css')}}">
    <!--[if lt IE 9]>
        <script src="{{asset("frontend/assets/js/html5shiv.min.js")}}"></script>
        <script src="{{asset("frontend/assets/js/respond.min.js")}}"></script>
    <![endif]-->
    <script src="https://kit.fontawesome.com/079863ebb3.js"></script>
   
   <!-- for blogpost -->
    <!-- <script src="{{asset('frontend/assets/js/plugins.min.js')}}"></script>
    <script src="{{asset('frontend/assets/js/app.min.js')}}"></script>
    <script src="{{asset('frontend/assets/js/configurator.min.js')}}"></script> -->

    <!-- single blog post -->
    <!-- <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>Material Style</title>
    <meta name="description" content="Material Style Theme">
    <link rel="shortcut icon" href="assets/img/favicon.png?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="{{asset('frontend/assets/css/preload.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/assets/css/plugins.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/assets/css/style.light-blue-500.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/assets/css/width-boxed.min.css')}}" id="ms-boxed" disabled=""> -->
    <!-- [if lt IE 9]>
        <script src="{{asset('frontend/assets/js/html5shiv.min.js')}}"></script>
        <script src="{{asset('frontend/assets/js/respond.min.js')}}"></script>
    <![endif] -->
   
  </head>