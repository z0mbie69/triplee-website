<aside class="ms-footbar">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 ms-footer-col">
          <div class="ms-footbar-block">
            <h3 class="ms-footbar-title">Sitemap</h3>
            <ul class="list-unstyled ms-icon-list three_cols">
              <li><a href="/"><i class="fas fa-home"></i> Home</a></li>
              <li><a href="/blog"><i class="fas fa-pen"></i> Blog</a></li>
              <li><a href="/portfolio"><i class="fas fa-image"></i> Portfolio</a></li>
              <!-- <li><a href="portfolio-filters_si"><i class="fas fa-suitcase-rolling"></i> Works</a></li> -->
              
              
              <li><a href="/about"><i class="fas fa-heart"></i> About Us</a></li>
              
              <li><a href="/service"><i class="fas fa-smile-wink"></i> Services</a></li>
              <!--  
              <li><a href=""><i class="fas fa-question"></i> FAQ</a></li>
              <li><a href="-l"><i class="fas fa-lock"></i> Login</a></li>
              <li><a href="-timeline_"><i class="fas fa-clock"></i> Timeline</a></li>
              <li><a href="-pr"><i class="fas fa-dollar-sign"></i> Pricing</a></li>
              <li><a href="fcontact"><i class="fas fa-envelope"></i> Contact</a></li>
              <li><a href="/fourteam"><i class="fas fa-user-friends"></i> Our Team</a></li>
            -->
            </ul>
          </div>
          <div class="ms-footbar-block">
            <h3 class="ms-footbar-title">Social Media</h3>
          
            <div class="ms-footbar-social">
          <!-- <li class="{{Request::is('/')?'nav-item active':'nav-item'}}"> -->

              <a href="{{$social_data->facebook_url}}" target="_blank"  class="btn-circle btn-facebook"><i class="fab fa-facebook-f"></i></a>
          </li>
              <a href="{{$social_data->twitter_url}}" target="_blank"  class="btn-circle btn-twitter"><i class="fab fa-twitter"></i></a>
              <!-- <a href="{{$social_data->youtube_url}}" target="_blank" class="btn-circle btn-youtube"><i class="fab fa-youtube"></i></a><br> -->
              <a href="{{$social_data->instagram_url}}" target="_blank" class="btn-circle btn-instagram"><i class="fab fa-instagram"></i></a>
              <a href="{{$social_data->github_url}}" target="_blank" class="btn-circle btn-github"><i class="fab fa-github"></i></a>
            </div>
           
          </div>
          <!--  
          <div class="ms-footbar-block">
            <h3 class="ms-footbar-title">Subscribe</h3>
            <p class="">Lorem ipsum Amet fugiat elit nisi anim mollit minim labore ut esse Duis ullamco ad dolor veniam velit.</p>
            <form>
              <div class="form-group label-floating mt-2 mb-1">
                <div class="input-group ms-input-subscribe">
                  <label class="control-label" for="ms-subscribe"><i class="fas fa-envelope"></i> Email Adress</label>
                  <input type="email" id="ms-subscribe" class="form-control">
                </div>
              </div>
              
              <button class="ms-subscribre-btn" type="button">Subscribe <i class="fas fa-envelope-open"></i></button>
            </form>
          </div>-->
        </div>
        <div class="col-lg-5 col-md-7 ms-footer-col ms-footer-alt-color">
          <div class="ms-footbar-block">
            <h3 class="ms-footbar-title text-center mb-2">Subscribe</h3>
            <div class="ms-footer-media">
              
            <div class="ms-footbar-block">
            <!-- <h3 class="ms-footbar-title">Subscribe</h3> -->
            <p class="">Subscribe to our Newsletter and always be updated about latest Technology trends.</p>
            <form>
              <div class="form-group label-floating mt-2 mb-1">
                <div class="input-group ms-input-subscribe">
                  <label class="control-label" for="ms-subscribe"><i class="fas fa-envelope"></i> Email Address</label>
                  <input type="email" id="ms-subscribe" class="form-control">
                </div>
              </div>
              
              <button class="ms-subscribre-btn" type="button">Subscribe <i class="fas fa-envelope-open"></i></button>
            </form>
          </div>
             
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-5 ms-footer-col ms-footer-text-right">
          <div class="ms-footbar-block">
            <div class="ms-footbar-title">
            <a href="{{Route('front.index')}}">
            <img src="{{asset('backend/uploads/image/'.$logo_data->limage)}}" style="width:50px; height:50px; border-radius:100px!important;"  >
              <!-- <h3 class="no-m ms-site-title">EEE<span class="innovation">Innovation</span></h3></a> -->
              <h3 class="no-m ms-site-title">{{$logo_data->name}}</h3></a>

            </div>
            <address class="no-mb">
              <p><i class="color-danger-light fas fa-map-marker-alt"></i> {{$contact_data->address1}}</p>
              <p><i class="color-warning-light fas fa-map"></i> {{$contact_data->address2}}</p>
              <p><i class="color-info-light fas fa-envelope"></i> <a href="mailto:{{$contact_data->email}}">{{$contact_data->email}}</a></p>
              <p><i class="color-royal-light fas fa-phone"></i> {{$contact_data->contact1}}</p>
              <p><i class="color-success-light fas fa-fax"></i> {{$contact_data->contact2}}</p>
            </address>
          </div>
          
        </div>
      </div>
    </div>
  </aside>
  <footer class="ms-footer">
    <div class="container">
      <p>Copyright &copy; {{$logo_data->name}} {{date('Y')}}</p>
    </div>
  </footer>
  <div class="btn-back-top">
    <a href="#" data-scroll id="back-top" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised "><i class="fas fa-arrow-up"></i></a>
  </div>