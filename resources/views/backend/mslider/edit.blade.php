@extends('backend.main')

@section('title','| MacSlider edit')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         Mac Slider Form Edit
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="iconsmind-Library"></i></a></li>
        <li class="breadcrumb-item"><a href="{{route('mslider.index')}}"> List all</a></li>
        <li class="breadcrumb-item active"><a href="{{route('mslider.create')}}"> Create</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h6 class="box-subtitle text-white">MacSlider Form edit </h6>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">
            {{Form::model($mslider,['route'=>['mslider.update',$mslider->id],'method'=>'PUT','files'=>'true','data-parsley-validate'=>''])}}
				
          

          <div class="form-group">
            <h5>Image:<span class="text-danger">*</span></h5>
            <div class="controls">
            <img src="{{asset('backend/uploads/image/'.$mslider->simage)}}" height="150" alt="">
       <input type="file" name="simage" class="form-control" > </div>
        </div>
    

      
            {{Form::submit('submit',['class'=>'form-control'])}}
            {{Form::close()}}
          </div>
        </div></div></div></section></div>
@endsection
