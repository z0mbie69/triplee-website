@extends('backend.main')
@section('title','| mslider list')
@section('content')

<div class="content-wrapper">
  <div class="content">
         <div class="box box-solid box-primary">
            <div class="box-header with-border">
              <h4 class="box-title">Mslider List Data Table</h4>
              <h6 class="box-subtitle text-white-50">Export data to Copy, CSV, Excel, PDF & Print</h6>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                  <tr>
                    <th>S.N</th>  
                    <th>Image</th>
                    
                   <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($mslider as $mslider)
                     <tr>
                       <td>{{$loop->index+1}}</td>
                     <td><img src="{{asset('backend/uploads/image/'.$mslider->simage)}}" height="50px"></td>
                      

                       <td>
                          {{-- <a href="{{route('mslider.edit',$mslider->id)}}" class="btn btn-primary">Edit</a> --}}
                          <a href="{{route('mslider.edit',$mslider->id)}}" class="icon"><i class="fa fa-edit fa-2x text-info"></i></a>&nbsp;&nbsp;
                          <a  href="javascript:" rel="{{$mslider->id}}" rel1="admin/mslider/destroy" class="text-inverse deleteRecord" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                            <i class="fa fa-trash fa-2x text-danger "></i>
                        </a>
                         
                         
        
                         
                         </td>
                     </tr>
                     @endforeach
                   </tbody>
				
				</table>
				</div>              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
</div>
</div>
@endsection










