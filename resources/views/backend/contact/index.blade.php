@extends('backend.main')

@section('content')

<div class="content-wrapper">
  <div class="content">
         <div class="box box-solid box-primary">
            <div class="box-header with-border">
              <h4 class="box-title">contact List Data Table</h4>
              <h6 class="box-subtitle text-white-50">Export data to Copy, CSV, Excel, PDF & Print</h6>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                  <tr>
                    <th>S.N</th>  
                    <th>Address1</th>
                    <th>Address2</th>
                    <th>email</th>
                   <th>Contact1</th>
                   <th>Contact2</th>
                   <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($contact as $contact)
                     <tr>
                       <td>{{$loop->index+1}}</td>
                       <td>{{$contact->address1}}</td>
                       <td>{{$contact->address2}}</td>
                       <td>{{$contact->email}}</td>
                       <td>{{$contact->contact1}}</td>
                       <td>{{$contact->contact2}}</td>
                    

                       <td>
                          <a href="{{route('contact.edit',$contact->id)}}" class="icon"><i class="fa fa-edit fa-2x text-info"></i></a>&nbsp;&nbsp;
                      
                         
                         
        
                         
                         </td>
                     </tr>
                     @endforeach
                   </tbody>
				
				</table>
				</div>              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
</div>
</div>
@endsection










