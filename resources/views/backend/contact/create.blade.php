@extends('backend.main')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Validation
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}"><i class="iconsmind-Library"></i></a></li>
        <li class="breadcrumb-item"><a href="{{Route('contact.index')}}">List all</a></li>
        <li class="breadcrumb-item active">Form Validation</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h6 class="box-subtitle text-white">Contact Form </a></h6>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">
              
      {!! Form::open(array('route'=>'contact.store','data-parsley-validate' => '','files' => 'true'))!!}        
     
      <div class="form-group">
      <div class="form-group">
                <h5>Address 1:<span class="text-danger">*</span></h5>
                <div class="controls">
                <Input type="text" name="address1" maxlength="20" class="form-control"  placeholder="Enter sub Address" required>
                </div>
              </div>

              <div class="form-group">
                <h5>Address 2:<span class="text-danger">*</span></h5>
                <div class="controls">
                <Input type="text" name="address2" maxlength="20" class="form-control"  placeholder="Enter main Address" required>
                </div>
              </div>

              <div class="form-group">
						<h5>Email: <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="email" name="email" maxlength="30" class="form-control" required data-validation-required-message="This field is required"> </div>
                    </div>
                    
                    <div class="form-group">
                <h5>contact Number(personal):<span class="text-danger">*</span></h5>
                <div class="controls">
                <Input type="string" name="contact1" maxlength="20" class="form-control"   required>
                </div>
              </div>

              <div class="form-group">
                <h5>contact Number(office):<span class="text-danger">*</span></h5>
                <div class="controls">
                <Input type="string" name="contact2" maxlength="20" class="form-control"  required>
                </div>
              </div>

          
         
              <div class= "box-footer">
         
            {{Form::submit('submit',['class'=>'form-control'])}}
          </div>
     
           <!-- /.box-footer-->
           {{ Form::close() }}
         </div>
         <!-- /.box -->
         

          </section>

 <!-- /.content -->
</div>
@endsection 