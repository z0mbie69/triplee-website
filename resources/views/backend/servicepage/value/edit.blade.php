@extends('backend.main')
@section('title','| Edit value')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      value Form Edit
    </h1>
    <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="iconsmind-Library"></i></a></li>
      <li class="breadcrumb-item"><a href="{{route('value.index')}}">List All</a></li>
      <li class="breadcrumb-item active"><a href="{{route('value.create')}}">Create</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
   
   <!-- Basic Forms -->
    <div class="box box-solid box-primary">
      <div class="box-header with-border">
        <h6 class="box-subtitle text-white">Edit Value</h6>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col">
            {{Form::model($value,['route'=>['value.update',$value->id],'method'=>'PUT','files'=>'true','data-parsley-validate'=>''])}}
      
           
        	
       
            <div class="form-group">
              <h5>Detail: <span class="text-danger">*</span></h5>
              <div class="controls">
                  <textarea name="detail"  id="editor1" rows="4" cols="50" placeholder="Describe yourself here..." required data-validation-required-message="This field is required">{{$value->detail}}</textarea> </div>
    
            </div>
     

      
            {{Form::submit('submit',['class'=>'form-control'])}}
            {{Form::close()}}
          </div>
        </div></div></div></section></div>
@endsection
