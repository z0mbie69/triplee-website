@extends('backend.main')

@section('title','| note')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         note Form Add
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="iconsmind-Library"></i></a></li>
        <li class="breadcrumb-item"><a href="{{route('note.index')}}">List all</a></li>
        <li class="breadcrumb-item active">Form Validation</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h6 class="box-subtitle text-white">Create Note</h6>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">
            {{Form::open(['route'=>'note.store','method'=>'POST','files'=>'true','data-parsley-validate'=>''])}}
				
      
   	
            <div class="form-group">
                <h5>Title: <span class="text-danger">*</span></h5>
                <div class="controls">
                  <textarea name="title" class="form-control" rows="4" cols="50" placeholder="Describe yourself here..." required data-validation-required-message="This field is required"> </textarea> </div>
              </div>

        <div class="form-group">
          <h5>Detail: <span class="text-danger">*</span></h5>
          <div class="controls">
							<textarea name="detail"  id="editor1" rows="4" cols="50" placeholder="Describe yourself here..." required data-validation-required-message="This field is required"></textarea> </div>

        </div>
     
  
           
                <div class="form-group">
              <h5>Image:<span class="text-danger">*</span></h5>
              <div class="controls">
                <input type="file" name="image" class="form-control" required> 
              </div>
                </div>
              </div>

       

      
            {{Form::submit('submit',['class'=>'form-control'])}}
            {{Form::close()}}
          </div>
        </div></div></div></section></div>
@endsection
