@extends('backend.main')

@section('content')

<div class="content-wrapper">
  <div class="content">
         <div class="box box-solid box-primary">
            <div class="box-header with-border">
              <h4 class="box-title">Blog Articles Data Table</h4>
              <h6 class="box-subtitle text-white-50">Export data to Copy, CSV, Excel, PDF & Print</h6>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                  <tr>
                    <th>S.N</th> 
                    <th>Image</th> 
                    <th>Title</th>
                    <th>Description</th>
                   <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($blog as $blog)
                     <tr>
                       <td>{{$loop->index+1}}</td>
                       <td><img src="{{asset('/backend/uploads/image/'.$blog->bimage)}}" height="75px" width="75px"></td>
                       <td>{{$blog->title}}</td>
                       <td>{{substr(strip_tags($blog->description),0,22)}}{{strlen($blog->description)>15?"...":""}}</td>
                       
                       <td>
                          <a href="{{route('blog.edit',$blog->id)}}" class="icon"><i class="fa fa-edit fa-2x text-info"></i></a>&nbsp;&nbsp;
                          <a  href="javascript:" rel="{{$blog->id}}" rel1="admin/blog/destroy" class="text-inverse deleteRecord" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                            <i class="fa fa-trash fa-2x text-danger "></i>
                        </a>                        
                         </td>


                     </tr>
                     @endforeach
                   </tbody>
				
				</table>
				</div>              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
</div>
</div>
@endsection










