@extends('backend.main')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Validation
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="iconsmind-Library"></i></a></li>
        <li class="breadcrumb-item"><a href="{{route('blog.create')}}">Create</a></li>
        <li class="breadcrumb-item active"><a href="{{route('blog.index')}}">List</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h6 class="box-subtitle text-white">Edit Blog</h6>
        </div>`
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">

         {!! Form::model($blog,array('route'=>['blog.update',$blog->id],'method'=>'PUT','data-parsley-validate' => '','files' => 'true'))!!}
        
         <div class="form-group">
						<h5>Image:<span class="text-danger">*</span></h5>
            <div class="controls">
            <img   src="{{asset('backend/uploads/image/'.$blog->bimage)}}" class="border border-dark rounded d-block" alt="photo" width="100" height="120">
              <input type="file" name="timage" value="{{$blog->bimage}}" class="form-control" required> </div>
					</div>

             
             <div class="form-group">
          <h5>Image Title: <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="image_title" value="{{$blog->image_title}}"  class="form-control" required data-validation-required-message="This field is required"> </div>
          </div>

          <div class="form-group">
						<h5>Image Description:<span class="text-danger">*</span></h5>
						<div class="controls">
              <textarea id="editor1"   name="image_description" class="form-control" required> {{$blog->image_description}}</textarea>
            </div>
          </div>
         

          <div class="form-group">

                <h5> Title: <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="title"  class="form-control" required data-validation-required-message="This field is required"> </div>
          </div>
          
        

          <div class="form-group">
						<h5>Description:<span class="text-danger">*</span></h5>
						<div class="controls">
              <textarea id="editor2"   name="description" class="form-control" required> {{$blog->description}}</textarea>
            </div>
          </div>
                 
      
              <div class="form-group">
                <label>Select Category</label>
                <select class="form-control select2" name="category[]" multiple="multiple"  data-placeholder="Select Category"
                        style="width: 100%;">
                  <option>Design</option>
                  <option>Graphics</option>
                  <option>Multimedia</option>
                  <option>Resource</option>
               
                </select>
              </div>
			
    
					

              <div class= "box-footer">
         
         {{Form::submit('submit',['class'=>'form-control'])}}
     
   
    </div>
     </div>

     {{ Form::close() }}
@endsection