@extends('backend.main')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Blog Create Form
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}"><i class="iconsmind-Library"></i></a></li>
        <li class="breadcrumb-item"><a href="{{Route('blog.index')}}">List Content</a></li>
        <li class="breadcrumb-item active">Form Validation</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h6 class="box-subtitle text-white">Create Blog </a></h6>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">
              
      {!! Form::open(array('route'=>'blog.store','data-parsley-validate' => '','files' => 'true'))!!}        
     
      <div class="form-group">

      

      <div class="form-group">
						<h5>Image:<span class="text-danger">*</span></h5>
						<div class="controls">
              <input type="file" name="bimage" class="form-control" required>
             </div>
             </div>

             
             <div class="form-group">
          <h5>Image Title: <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="image_title"  class="form-control" required data-validation-required-message="This field is required"> </div>
          </div>

          <div class="form-group">
          <h5>Image Description: <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="image_description"  class="form-control" required data-validation-required-message="This field is required"> </div>
          </div>
          </div>

          <div class="form-group">

                <h5> Title: <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="title"  class="form-control" required data-validation-required-message="This field is required"> </div>
          </div>
          
        

          <div class="form-group">
						<h5>Description:<span class="text-danger">*</span></h5>
						<div class="controls">
              <textarea name="description" id="editor1" class="form-control" required> </textarea>
            </div>
            </div>
                 
      
              <div class="form-group">
                <label>Select Category</label>
                <select class="form-control select2" name="category[]" multiple="multiple"  data-placeholder="Select Category"
                        style="width: 100%;">
                  <option>Design</option>
                  <option>Graphics</option>
                  <option>Multimedia</option>
                  <option>Resource</option>
               
                </select>
              </div>

              <!-- /.form-group -->
         
            <!-- /.col -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
          
            <div class= "box-footer">
         
          {{Form::submit('submit',['class'=>'form-control'])}}
      
    
     </div>
    
    

           <!-- /.box-footer-->
           {{ Form::close() }}
         </div>
         <!-- /.box -->
         

          </section>

 <!-- /.content -->
</div>
@endsection 