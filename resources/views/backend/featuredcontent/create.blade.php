@extends('backend.main')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Validation
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}"><i class="iconsmind-Library"></i></a></li>
        <li class="breadcrumb-item"><a href="{{Route('blog.index')}}">List All</a></li>
        <li class="breadcrumb-item active">Form Validation</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h6 class="box-subtitle text-white">
           Create Featured Content </a></h6>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">
              
      {!! Form::open(array('route'=>'featured.store','data-parsley-validate' => '','files' => 'true'))!!}        
     

          
					<div class="form-group">
						<h5>Image: <span class="text-danger">*</span></h5>
						<div class="controls">
              <input type="file" name="fimage1" class="form-control" required>
             </div>
             </div>

             <div class="form-group">
						<h5> Label: <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="label1"  class="form-control" required data-validation-required-message="This field is required"> </div>
          </div>

          <div class="form-group">
          <h5>Description: <span class="text-danger">*</span></h5>
          <div class="controls">
							<textarea name="description1"  id="editor1" rows="4" cols="50" placeholder="Describe yourself here..." required data-validation-required-message="This field is required"></textarea> </div>

        </div>
          
            <div class= "box-footer">
          {{Form::submit('submit',['class'=>'form-control'])}}      
    
     </div>
    
           <!-- /.box-footer-->
           {{ Form::close() }}
         </div>
         <!-- /.box -->
         

          </section>

 <!-- /.content -->
</div>
@endsection 