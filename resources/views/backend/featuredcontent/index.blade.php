@extends('backend.main')

@section('content')

<div class="content-wrapper">
  <div class="content">
         <div class="box box-solid box-primary">
            <div class="box-header with-border">
              <h4 class="box-title">Featured Content Data Table</h4>
              <h6 class="box-subtitle text-white-50">Export data to Copy, CSV, Excel, PDF & Print</h6>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                  <tr>
                    <th>S.N</th>  
                    <th>Image</th>
                    <th>Label</th>
                    <th>Description</th>
                   <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($featuredcontent as $featuredcontent)
                     <tr>
                       <td>{{$loop->index+1}}</td>
                       <td><img src="{{asset('/backend/uploads/image/'.$featuredcontent->fimage1)}}" height='70px' width='70px'></td>
                       <td>{{substr(strip_tags($featuredcontent->label1),0,22)}}{{strlen($featuredcontent->label1)>15?"...":""}}</td>
                       <td>{{substr(strip_tags($featuredcontent->description1),0,22)}}{{strlen($featuredcontent->description1)>15?"...":""}}</td>
                       <td>
                          <a href="{{route('featured.edit',$featuredcontent->id)}}" class="icon"><i class="fa fa-edit fa-2x text-info"></i></a>&nbsp;&nbsp;
                          <a  href="javascript:" rel="{{$featuredcontent->id}}" rel1="admin/featured/destroy" class="text-inverse deleteRecord" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                            <i class="fa fa-trash fa-2x text-danger "></i>
                        </a>                        
                         </td>
                     
                        </tr>
                     @endforeach
                   </tbody>
				
				</table>
				</div>              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
</div>
</div>
@endsection










