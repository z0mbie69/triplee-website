@extends('backend.main')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Validation
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}"><i class="iconsmind-Library"></i></a></li>
        <li class="breadcrumb-item"><a href="{{Route('featured.index')}}">List</a></li>
        <li class="breadcrumb-item active"><a href="{{Route('featured.create')}}">Create</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h6 class="box-subtitle text-white">Featured Content Edit Form</h6>
        </div>`
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">

         {!! Form::model($featuredcontent,array('route'=>['featured.update',$featuredcontent->id],'method'=>'PUT','data-parsley-validate' => '','files' => 'true'))!!}
        
         <div class="form-group">
						<h5>Image 1:<span class="text-danger">*</span></h5>
            <div class="controls">
            <img   src="{{asset('backend/uploads/image/'.$featuredcontent->fimage1)}}" class="border border-dark rounded d-block" alt="photo" width="100" height="120">
              <input type="file" name="fimage1" value="{{$featuredcontent->fimage1}}" class="form-control" required> </div>
					</div>

             <div class="form-group">
						<h5> Label 1: <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="label1" value="{{$featuredcontent->label1}}" class="form-control" required data-validation-required-message="This field is required"> </div>
          </div>

          <div class="form-group">
          <h5>Description 1: <span class="text-danger">*</span></h5>
          <div class="controls">
							<textarea name="description1"  id="editor1" rows="4" cols="50" placeholder="Describe yourself here..." required data-validation-required-message="This field is required">{{$featuredcontent->description1}}</textarea> </div>

        </div>
  
              
            <div class= "box-footer">
          
          {{Form::submit('submit',['class'=>'form-control'])}}
      
     
     </div>

     {{ Form::close() }}
@endsection