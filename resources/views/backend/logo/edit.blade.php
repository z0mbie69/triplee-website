@extends('backend.main')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Logo Form
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}"><i class="iconsmind-Library"></i></a></li>
        <li class="breadcrumb-item"><a href="{{Route('logo.index')}}">List</a></li>
        <li class="breadcrumb-item active">Form Validation</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <!-- Basic Forms -->
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h6 class="box-subtitle text-white">Logo</h6>
        </div>`
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">

         {!! Form::model($logo,array('route'=>['logo.update',$logo->id],'method'=>'PUT','data-parsley-validate' => '','files' => 'true'))!!}
        
         <div class="form-group">
			<h5>Image:<span class="text-danger">*</span></h5>
            <div class="controls">
            <img   src="{{asset('backend/uploads/image/'.$logo->limage)}}" class="border border-dark rounded d-block" alt="photo" width="100" height="120">
              <input type="file" name="limage" value="{{$logo->limage}}" class="form-control" required> </div>
          </div>

          
          <div class="form-group">
          <h5>Name of Company: <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="name"  class="form-control" > </div>
          </div>
          


              <div class= "box-footer">
         
         {{Form::submit('submit',['class'=>'form-control'])}}
     
   
    </div>
     </div>

     {{ Form::close() }}
@endsection