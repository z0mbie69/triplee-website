@extends('backend.main')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Logo 
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}"><i class="iconsmind-Library"></i></a></li>
        <li class="breadcrumb-item"><a href="{{Route('logo.index')}}">List Content</a></li>
        <li class="breadcrumb-item active">logo</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h6 class="box-subtitle text-white">Create logo</a></h6>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">
              
      {!! Form::open(array('route'=>'logo.store','data-parsley-validate' => '','files' => 'true'))!!}        
     
      <div class="form-group">
      <h5>Logo:<span class="text-danger">*</span></h5>
						<div class="controls">
              <input type="file" name="limage" class="form-control" required>
             </div>
      <br>

      <div class="form-group">
					

             <div class="form-group">
          <h5>Name of Company: <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="name"  class="form-control" required data-validation-required-message="This field is required"> </div>
          </div>
             </div>        
          </div>      
        
              <!-- /.form-group -->
         
            <!-- /.col -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
          
            <div class= "box-footer">
         
          {{Form::submit('submit',['class'=>'form-control'])}}
      
    
     </div>
    
    

           <!-- /.box-footer-->
           {{ Form::close() }}
         </div>
         <!-- /.box -->
         

          </section>

 <!-- /.content -->
</div>
@endsection 