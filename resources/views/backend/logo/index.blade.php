@extends('backend.main')

@section('content')

<div class="content-wrapper">

  <div class="content">
         <div class="box box-solid box-primary">
            <div class="box-header with-border">
              <h4 class="box-title">logo Table</h4>
              <h6 class="box-subtitle text-white-50">Export data to Copy, CSV, Excel, PDF & Print</h6>          
            </div>
    
            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                  <tr>
                    <th>S.N</th> 
                    <th>Image</th> 
                    <th>Name</th>
                   <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($logo as $logo)
                     <tr>
                       <td>{{$loop->index+1}}</td>
                       <td><img src="{{asset('/backend/uploads/image/'.$logo->limage)}}" height="75px" width="75px"></td>
                       <td>{{$logo->name}}</td>
                       
                       
                       <td>
                          <a href="{{route('logo.edit',$logo->id)}}" class="icon"><i class="fa fa-edit fa-2x text-info"></i></a>&nbsp;&nbsp;
                                              
                         </td>


                     </tr>
                     @endforeach
                   </tbody>
				
				</table>

        <ol class="breadcrumb">
        <!-- <li class="breadcrumb-item"><a href="{{Route('logo.create')}}">Create New</a></li>      -->
      </ol>
				</div>              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
</div>
</div>
@endsection










