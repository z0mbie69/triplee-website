@extends('backend.main')

@section('content')

<div class="content-wrapper">
  <div class="content">
         <div class="box box-solid box-primary">
            <div class="box-header with-border">
              <h4 class="box-title">abutton List Data Table</h4>
              <h6 class="box-subtitle text-white-50">Export data to Copy, CSV, Excel, PDF & Print</h6>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                  <tr>
                    <th>S.N</th>  
                    <th>Title</th>
                    <th>Title Icon</th>
                    <th>Buttom Icon</th>
                    <th>Detail</th>
                   <th>Action</th>  
                  </tr>
                </thead>
                <tbody>
                    @foreach($abutton as $abutton)
                     <tr>
                       <td>{{$loop->index+1}}</td>
                       <td>{{$abutton->title}}</td>
                       <td>{{$abutton->ticon}}</td>
                       <td>{{$abutton->bicon}}</td>
                     {{-- <td>{!!substr($abutton->detail,0,5)!!}{{strlen($abutton->detail)>2?"...":""}}</td> --}}
                     <td>{{substr(strip_tags($abutton->detail),0,12)}}{{strlen($abutton->detail)>15?"...":""}}</td>

                       <td>
                          {{-- <a href="{{route('abutton.edit',$abutton->id)}}" class="btn btn-primary">Edit</a> --}}
                          <a href="{{route('abutton.edit',$abutton->id)}}" class="icon"><i class="fa fa-edit fa-2x text-info"></i></a>&nbsp;&nbsp;
                          <a  href="javascript:" rel="{{$abutton->id}}" rel1="admin/abutton/destroy" class="text-inverse deleteRecord" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                            <i class="fa fa-trash fa-2x text-danger "></i>
                        </a>
                         
                         
        
                         
                         </td>
                     </tr>
                     @endforeach
                   </tbody>
				
				</table>
				</div>              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
</div>
</div>
@endsection










