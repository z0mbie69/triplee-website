@extends('backend.main')
@section('title','| Edit numeric')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      numeric Form Edit
    </h1>
    <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="iconsmind-Library"></i></a></li>
      <li class="breadcrumb-item"><a href="{{route('numeric.create')}}">Create</a></li>
      <li class="breadcrumb-item active"><a href="{{route('numeric.index')}}">List</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
   
   <!-- Basic Forms -->
    <div class="box box-solid box-primary">
      <div class="box-header with-border">
        <h6 class="box-subtitle text-white">Edit Numeric Form</h6>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col">
            {{Form::model($numeric,['route'=>['numeric.update',$numeric->id],'method'=>'PUT','files'=>'true','data-parsley-validate'=>''])}}
      
            <div class="form-group">
                <h5>Title:<span class="text-danger">*</span></h5>
                <div class="controls">
                <Input type="text" name="title" value="{{$numeric->title}}"class="form-control"  placeholder="{{$numeric->title}}" required>
                </div>
              </div>


        <div class="form-group">
          <h5>Number: <span class="text-danger">*</span></h5>
          <div class="controls">
              <Input type="number" name="number" value="{{$numeric->number}}"class="form-control"  placeholder="{{$numeric->number}}" required>

        </div>

        <div class="form-group">
            <h5>Icon:<span class="text-danger">*</span></h5>
            <div class="controls">
              <Input type="text"  value="{{$numeric->icon}}"name="icon" class="form-control"  placeholder=" {{$numeric->icon}}" required>
               
            </div>
          </div>

      
            {{Form::submit('submit',['class'=>'form-control'])}}
            {{Form::close()}}
          </div>
        </div></div></div></section></div>
@endsection
