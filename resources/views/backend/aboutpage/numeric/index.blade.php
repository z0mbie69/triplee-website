@extends('backend.main')

@section('content')

<div class="content-wrapper">
  <div class="content">
         <div class="box box-solid box-primary">
            <div class="box-header with-border">
              <h4 class="box-title">numeric List Data Table</h4>
              <h6 class="box-subtitle text-white-50">Export data to Copy, CSV, Excel, PDF & Print</h6>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                  <tr>
                    <th>S.N</th>  
                    <th>Title</th>
                    <th>Icon</th>
                   <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($numeric as $numeric)
                     <tr>
                       <td>{{$loop->index+1}}</td>
                       <td>{{$numeric->title}}</td>
                       <td>{{$numeric->icon}}</td>
                     {{-- <td>{!!substr($numeric->description,0,5)!!}{{strlen($numeric->description)>2?"...":""}}</td> --}}

                       <td>
                          {{-- <a href="{{route('numeric.edit',$numeric->id)}}" class="btn btn-primary">Edit</a> --}}
                          <a href="{{route('numeric.edit',$numeric->id)}}" class="icon"><i class="fa fa-edit fa-2x text-info"></i></a>&nbsp;&nbsp;
                          <a  href="javascript:" rel="{{$numeric->id}}" rel1="dmin/numeric/destroy" class="text-inverse deleteRecord" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                            <i class="fa fa-trash fa-2x text-danger "></i>
                        </a>
                         
                         
        
                         
                         </td>
                     </tr>
                     @endforeach
                   </tbody>
				
				</table>
				</div>              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
</div>
</div>
@endsection










