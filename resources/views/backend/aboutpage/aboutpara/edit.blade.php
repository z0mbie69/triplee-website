@extends('backend.main')

@section('title','| aboutpara')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         aboutpara Form Edit
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="iconsmind-Library"></i></a></li>
        <li class="breadcrumb-item"><a href="{{route('aboutpara.create')}}">Create</a></li>
        <li class="breadcrumb-item active"><a href="{{route('aboutpara.index')}}">List</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h6 class="box-subtitle text-white">Edit About Paragraph</h6>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">
            {{Form::model($aboutpara,['route'=>['aboutpara.update',$aboutpara->id],'method'=>'PUT','files'=>'true','data-parsley-validate'=>''])}}
				
          


        <div class="form-group">
          <h5>Detail: <span class="text-danger">*</span></h5>
          <div class="controls">
		<textarea name="detail"  id="editor1" rows="15" cols="100" placeholder="Describe yourself here..." required data-validation-required-message="This field is required">{{$aboutpara->detail}}
            </textarea> 
        </div>

        </div>


          <div class="form-group">
                <h5>category<span class="text-danger">*</span></h5>
                <select name="category" id="category" class="form-control">
                <option >{{$aboutpara->category}}</option>
                    <option name="category">aboutparagraph</option>
                   
                    <option name="category">visionparagraph</option>
                 </select>
            </div>

      
            {{Form::submit('submit',['class'=>'form-control'])}}
            {{Form::close()}}
          </div>
        </div></div></div></section></div>
@endsection