@extends('backend.main')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Validation
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="iconsmind-Library"></i></a></li>
        <li class="breadcrumb-item"><a href="{{route('copyright.create')}}">Create</a></li>
        <li class="breadcrumb-item active"><a href="{{route('copyright.index')}}">List</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h6 class="box-subtitle text-white">Edit Copyright</h6>
        </div>`
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">

         {!! Form::model($copyright,array('route'=>['copyright.update',$copyright->id],'method'=>'PUT','data-parsley-validate' => '','files' => 'true'))!!}
                     
             <div class="form-group">
          <h5>Name: <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="name" value="{{$copyright->name}}"  class="form-control" required data-validation-required-message="This field is required"> </div>
          </div>

          <div class="form-group">

                <h5> year: <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="year" value="{{$copyright->year}}" class="form-control" required data-validation-required-message="This field is required"> </div>
          </div>
           <div class= "box-footer">
         
         {{Form::submit('submit',['class'=>'form-control'])}}
     
   
    </div>
     </div>

     {{ Form::close() }}
@endsection