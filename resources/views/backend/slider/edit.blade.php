@extends('backend.main')

@section('title','| Slider')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         Slider Form Edit
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="iconsmind-Library"></i></a></li>
        <li class="breadcrumb-item"><a href="{{route('slider.index')}}"> List All</a></li>
        <li class="breadcrumb-item active"><a href="{{route('value.create')}}">Create</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h6 class="box-subtitle text-white">Edit Slider </h6>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">
            {{Form::model($slider,['route'=>'slider.store','method'=>'POST','files'=>'true','data-parsley-validate'=>''])}}
				
            <div class="form-group">
                <h5>Title:<span class="text-danger">*</span></h5>
                <div class="controls">
                <Input type="text" name="stitle" value="{{$slider->stitle}}"class="form-control"  placeholder="slider title...." required>
                </div>
              </div>


        <div class="form-group">
          <h5>Slider Detail: <span class="text-danger">*</span></h5>
          <div class="controls">
							<textarea name="sdetail"  id="editor1" rows="4" cols="50" placeholder="Slider Detail..." required data-validation-required-message="This field is required">{{$slider->sdetail}}</textarea> </div>

        </div>

        <div class="form-group">
            <h5>Icon:<span class="text-danger">*</span></h5>
            <div class="controls">
            <Input type="text" name="i1name" class="form-control" value="{{$slider->i1name}}" placeholder="slider icon name...." required>
            </div>
          </div>

          <div class="form-group">
            <h5>Icon Detail: <span class="text-danger">*</span></h5>
            <div class="controls">
                              <textarea name="i1detail"  id="editor2" rows="4" cols="50" placeholder="Describe icon detail..." required data-validation-required-message="This field is required">{{$slider->i1detail}}</textarea> </div>
  
          </div>
          <div class="form-group">
              <h5>Icon:<span class="text-danger">*</span></h5>
              <div class="controls">
              <Input type="text" name="i2name" class="form-control" value="{{$slider->i2name}}" placeholder="slider icon name...." required>
              </div>
            </div>
  
            <div class="form-group">
              <h5>Icon Detail: <span class="text-danger">*</span></h5>
              <div class="controls">
                                <textarea name="i2detail"  id="editor3" rows="4" cols="50" placeholder="Describe icon detail..." required data-validation-required-message="This field is required">{{$slider->i2detail}}</textarea> </div>
    
            </div>
            <div class="form-group">
                <h5>Icon:<span class="text-danger">*</span></h5>
                <div class="controls">
                <Input type="text" name="i3name" class="form-control" value="{{$slider->i3name}}"  placeholder="slider icon name...." required>
                </div>
              </div>
    
              <div class="form-group">
                <h5>Icon Detail: <span class="text-danger">*</span></h5>
                <div class="controls">
                                  <textarea name="i3detail"  id="editor4" rows="4" cols="50" placeholder="Describe icon detail..." required data-validation-required-message="This field is required">{{$slider->i3detail}}</textarea> </div>
      
              </div>

          {{-- <div class="form-group">
            <h5>Image:<span class="text-danger">*</span></h5>
            <div class="controls">
            <img src="{{asset('backend/uploads/image/'.$slider->simage)}}" alt="">
       <input type="file" name="simage" class="form-control" > </div>
        </div> --}}
        {{-- <div class="form-group">
                <h5>Image2:<span class="text-danger">*</span></h5>
                <div class="controls">
                <img src="{{asset('backend/uploads/image/'.$slider->simage2)}}" alt="">
           <input type="file" name="simage2" class="form-control" required> </div>
            </div>  
            <div class="form-group">
                    <h5>Image3:<span class="text-danger">*</span></h5>
                    <div class="controls">
                    <img src="{{asset('backend/uploads/image/'.$slider->simage3)}}" alt="">
               <input type="file" name="simage3" class="form-control" required> </div>
                </div> --}}

      
            {{Form::submit('submit',['class'=>'form-control'])}}
            {{Form::close()}}
          </div>
        </div></div></div></section></div>
@endsection
