@extends('backend.main')

@section('content')

<div class="content-wrapper">
  <div class="content">
         <div class="box box-solid box-primary">
            <div class="box-header with-border">
              <h4 class="box-title">slider List Data Table</h4>
              <h6 class="box-subtitle text-white-50">Export data to Copy, CSV, Excel, PDF & Print</h6>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                  <tr>
                    <th>S.N</th>  
                    <th>Title</th>
                    <th>TDetail</th>
                    <th>I1</th>
                    <th>I1d</th>
                    <th>I2</th>
                    <th>I2d</th>
                    <th>I3</th>
                    <th>I3d</th>
                   <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($slider as $slider)
                     <tr>
                       <td>{{$loop->index+1}}</td>
                       <td>{{$slider->stitle}}</td>
                       <td>{!!$slider->sdetail!!}</td>
                     <td>{{$slider->i1name}}</td>
                     {{-- <td>{!!substr($slider->description,0,5)!!}{{strlen($slider->description)>2?"...":""}}</td> --}}
                     <td>{!!$slider->i1detail!!}{{strlen($slider->i1detail)>2?"...":""}}</td>
                     <td>{{$slider->i2name}}</td>
                     <td>{!!$slider->i2detail!!}{{strlen($slider->i2detail)>2?"...":""}}</td>
                     <td>{{$slider->i3name}}</td>
                     <td>{!!$slider->i3detail!!}{{strlen($slider->i3detail)>2?"...":""}}</td>

                       <td>
                          {{-- <a href="{{route('slider.edit',$slider->id)}}" class="btn btn-primary">Edit</a> --}}
                          <a href="{{route('slider.edit',$slider->id)}}" class="icon"><i class="fa fa-edit fa-2x text-info"></i></a>&nbsp;&nbsp;
                          <a  href="javascript:" rel="{{$slider->id}}" rel1="admin/slider/destroy" class="text-inverse deleteRecord" title="Delete" data-toggle="tooltip" data-original-title="Delete">
                            <i class="fa fa-trash fa-2x text-danger "></i>
                        </a>
                         
                         
        
                         
                         </td>
                     </tr>
                     @endforeach
                   </tbody>
				
				</table>
				</div>              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
</div>
</div>
@endsection










