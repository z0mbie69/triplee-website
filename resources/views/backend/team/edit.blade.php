@extends('backend.main')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Validation
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}"><i class="iconsmind-Library"></i></a></li>
        <li class="breadcrumb-item"><a href="{{Route('team.create')}}">create</a></li>
        <li class="breadcrumb-item active"><a href="{{Route('team.index')}}">list</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h6 class="box-subtitle text-white">Edit Team</h6>
        </div>`
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">

         {!! Form::model($team,array('route'=>['team.update',$team->id],'method'=>'PUT','data-parsley-validate' => '','files' => 'true'))!!}
        
         <div class="form-group">
						<h5>Name: <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="name" value="{{$team->name}}" class="form-control" required data-validation-required-message="This field is required my nigga"> </div>
					</div>

					<div class="form-group">
						<h5>Image:<span class="text-danger">*</span></h5>
            <div class="controls">
            <img   src="{{asset('backend/uploads/image/'.$team->timage)}}" class="border border-dark rounded d-block" alt="photo" width="100" height="120">
              <input type="file" name="timage" value="{{$team->timage}}" class="form-control" required> </div>
					</div>
          <div class="form-group">
						<h5>Description:<span class="text-danger">*</span></h5>
						<div class="controls">
              <textarea id="editor1"   name="description" class="form-control" required> {{$team->description}}</textarea>
            </div>

            <div class="form-group">
            <h5>Select Category<span class="text-danger">*</span></h5>
            <select name="category" id="category" value="{{$team->category}}"class="form-control">
                <option name="room">CEO</option>
                <option name="room">Developer</option>
                <option name="room">Social Media</option>
                <option name="room">Marketing</option>
                <option name="room">Design</option>

            </select>
        </div>
            

            <div class="form-group">
						<h5>Enter facebook URL <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="facebook_url" value="{{$team->facebook_url}}" class="form-control"  data-validation-regex-regex="((http[s]?|ftp[s]?):\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*" data-validation-regex-message="Only Valid URL's">
							<div class="form-control-feedback">  </div>
						</div>
					</div>
              
          <div class="form-group">
						<h5>Enter twitter URL <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="twitter_url" value="{{$team->twitter_url}}" class="form-control" data-validation-regex-regex="((http[s]?|ftp[s]?):\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*" data-validation-regex-message="Only Valid URL's">
							<div class="form-control-feedback"></div>
						</div>
          </div>
          
          <div class="form-group">
						<h5>Enter instagram URL <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="instagram_url" value="{{$team->instagram_url}}" class="form-control"  data-validation-regex-regex="((http[s]?|ftp[s]?):\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*" data-validation-regex-message="Only Valid URL's">
							<div class="form-control-feedback"></div>
						</div>
					</div>

            <div class= "box-footer">
          
    <button type="submit" class="btn btn-success"> submit

    </button>
      
     </div>
     </div>

     {{ Form::close() }}
@endsection