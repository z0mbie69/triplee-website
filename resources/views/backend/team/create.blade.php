@extends('backend.main')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Validation
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{Route('home')}}"><i class="iconsmind-Library"></i></a></li>
        <li class="breadcrumb-item"><a href="{{Route('team.index')}}">List all</a></li>
        <li class="breadcrumb-item active"><a href="{{Route('team.create')}}">Create</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box box-solid box-primary">
        <div class="box-header with-border">
          <h6 class="box-subtitle text-white">Our Team </a></h6>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col">
              
      {!! Form::open(array('route'=>'team.store','data-parsley-validate' => '','files' => 'true'))!!}        
     
      <div class="form-group">
						<h5> Name: <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="name"  class="form-control" required data-validation-required-message="This field is required"> </div>
          </div>
          
					<div class="form-group">
						<h5>Image:<span class="text-danger">*</span></h5>
						<div class="controls">
              <input type="file" name="timage" class="form-control" required>
             </div>
					</div>

          <div class="form-group">
						<h5>Description:<span class="text-danger">*</span></h5>
						<div class="controls">
              <input type="string" name="description" class="form-control" required> 
            </div>
            </div>

            <div class="form-group">
            <h5>Select Category<span class="text-danger">*</span></h5>
            <select name="category" id="category" class="form-control">
                <option name="room">CEO</option>
                <option name="room">Developer</option>
                <option name="room">Social Media</option>
                <option name="room">Marketing</option>
                <option name="room">Design</option>

            </select>
        </div>
          
            <div class="form-group">
						<h5>Enter facebook URL <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="facebook_url" class="form-control"  data-validation-regex-regex="((http[s]?|ftp[s]?):\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*" data-validation-regex-message="Only Valid URL's">
							<div class="form-control-feedback">               
              </div>
						</div>
					</div>
              
          <div class="form-group">
						<h5>Enter twitter URL <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="twitter_url" class="form-control" data-validation-regex-regex="((http[s]?|ftp[s]?):\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*" data-validation-regex-message="Only Valid URL's">
							<div class="form-control-feedback">
              </div>
						</div>
          </div>
          
          <div class="form-group">
						<h5>Enter instagram URL <span class="text-danger">*</span></h5>
						<div class="controls">
							<input type="text" name="instagram_url" class="form-control"  data-validation-regex-regex="((http[s]?|ftp[s]?):\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*" data-validation-regex-message="Only Valid URL's">
							<div class="form-control-feedback">
              </div>
						</div>
					</div>

          
            <div class= "box-footer">
          <div class = "row col-md-3">
         {{Form::submit('Submit', ['class'=>'btn btn-primary btn-sm'])}}
      
     </div>
     </div>
    
    

           <!-- /.box-footer-->
           {{ Form::close() }}
         </div>
         <!-- /.box -->
         

          </section>

 <!-- /.content -->
</div>
@endsection 