<!DOCTYPE html>
<html lang="en">
<head>
    @include('backend.partials._header')

</head> 

<body class="hold-transition skin-blue-light sidebar-mini">

    <div class="wrapper">
   
        @include('backend.partials._navbar')


        @include('backend.partials._sidebar')

        @yield('content')

        @include('backend.partials.message')
     
    @include('backend.partials._footer')
    <div class="control-sidebar-bg"></div>


    </div>
        

@yield('scripts')
    


    @include('backend.partials._script')
	
</body>
</html>
