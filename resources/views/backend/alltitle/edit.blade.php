@extends('backend.main')
@section('title','| Edit alltitle')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Alltitle Form Edit
    </h1>
    <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="iconsmind-Library"></i></a></li>
      <li class="breadcrumb-item"><a href="{{route('alltitle.create')}}">Create</a></li>
      <li class="breadcrumb-item active"><a href="{{route('alltitle.index')}}">List</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
   
   <!-- Basic Forms -->
    <div class="box box-solid box-primary">
      <div class="box-header with-border">
        <h6 class="box-subtitle text-white">Edit All Titles</h6>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col">
            {{Form::model($alltitle,['route'=>['alltitle.update',$alltitle->id],'method'=>'PUT','files'=>'true','data-parsley-validate'=>''])}}
      
            <div class="form-group">
                <h5>Title:<span class="text-danger">*</span></h5>
                <div class="controls">
                <Input type="text" name="title" value="{{$alltitle->title}}" class="form-control"  placeholder="{{$alltitle->title}}" required>
                </div>
              </div>


        <div class="form-group ">
          <h5>Detail: <span class="text-danger">*</span></h5>
          <div class="controls">
              <textarea name="detail" id="editor1" row="5" col="50" placeholder="{{$alltitle->detail}}" class="form-control" required>
                {{$alltitle->detail}}
              </textarea>
           </div>
        </div>

   

      <div class="form-group">
            <h5>category: <span class="text-danger">*</span></h5>
            <select name="category" id="category" class="form-control">

                <option name="category">{{$alltitle->category}}</option>
                <option name="category">About</option>
                <option name="category">Service</option>
                <option name="category">Portfolio</option>
                <option name="category">Our Team</option>
                <option name="category">Blog</option>
             </select>
        </div>

      
            {{Form::submit('submit',['class'=>'form-control'])}}
            {{Form::close()}}
          </div>
        </div></div></div></section></div>
@endsection
