


  <!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar-->
  <section class="sidebar">
    
    <!-- sidebar menu-->
    <ul class="sidebar-menu" data-widget="tree">
    
      <li class="treeview">
        <a href="#">
          <i class="simple-icon-note"></i>
          <span>Service</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('service.index')}}"><i class="iconsmind-Arrow-Through"></i>List</a></li>
         
            <li><a href="{{route('service.create')}}"><i class="iconsmind-Arrow-Through"></i>create</a></li> 
       
    </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="iconsmind-End-2"></i>
          <span>slider</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('slider.index')}}"><i class="iconsmind-Arrow-Through"></i>List</a></li>
         
            <li><a href="{{route('slider.create')}}"><i class="iconsmind-Arrow-Through"></i>create</a></li> 
       
    </ul>
      </li>

    
    
    <!-- <li class="treeview">
      <a href="#">
        <i class="iconsmind-Air-Balloon"></i>
        <span>Team</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-right pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{route('team.index')}}"><i class="iconsmind-Arrow-Through"></i>List</a></li>
       
          <li><a href="{{route('team.create')}}"><i class="iconsmind-Arrow-Through"></i>create</a></li> 
     
  </ul>
    </li> -->


    <li class="treeview">
      <a href="#">
        <i class="iconsmind-Cloud-Computer"></i>
        <span>Macslider</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-right pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        
        <li><a href="{{route('mslider.index')}}"><i class="iconsmind-Arrow-Through"></i>List</a></li>
       
          <li><a href="{{route('mslider.create')}}"><i class="iconsmind-Arrow-Through"></i>create</a></li> 
     
  </ul>
    </li>

  </li>
  
  <li class="treeview">
    <a href="#">
      <i class="iconsmind-Aa"></i>
      <span>All Title</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-right pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{route('alltitle.index')}}"><i class="iconsmind-Arrow-Through"></i>List</a></li>
     
        <li><a href="{{route('alltitle.create')}}"><i class="iconsmind-Arrow-Through"></i>create</a></li> 
   
</ul>
  </li>

    
      <li class="treeview">
        <a href="#">
          <i class="iconsmind-Add-SpaceBeforeParagraph"></i>
          <span>About paragraph</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-right pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('aboutpara.index')}}"><i class="iconsmind-Arrow-Through"></i>create</a></li>
         
            <!-- <li><a href="{{route('aboutpara.create')}}"><i class="iconsmind-Arrow-Through"></i>create</a></li>  -->
  
        </ul>
      
         
      </li>


      
      <li class="treeview">
          <a href="#">
            <i class="iconsmind-Numbering-List"></i>
            <span>About Numeric</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('numeric.index')}}"><i class="iconsmind-Arrow-Through"></i>List</a></li>
           
              <li><a href="{{route('numeric.create')}}"><i class="iconsmind-Arrow-Through"></i>create</a></li> 
    
          </ul>
        
           
        </li>

        <li class="treeview">
          <a href="#">
            <i class="iconsmind-Handshake"></i>
            <span>About approach</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('approach.index')}}"><i class="iconsmind-Arrow-Through"></i>List</a></li>
           
              <li><a href="{{route('approach.create')}}"><i class="iconsmind-Arrow-Through"></i>create</a></li> 
    
          </ul>
        
           
        </li>

        <li class="treeview">
            <a href="#">
              <i class="iconsmind-Cursor-Click"></i>
              <span>About Abutton</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{route('abutton.index')}}"><i class="iconsmind-Arrow-Through"></i>List</a></li>
             
                <li><a href="{{route('abutton.create')}}"><i class="iconsmind-Arrow-Through"></i>create</a></li> 
      
            </ul>
          
             
          </li>
          <li class="treeview">
            <a href="#">
              <i class="iconsmind-Dollar-Sign2"></i>
              <span>Service value</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{route('value.index')}}"><i class="iconsmind-Arrow-Through"></i>List</a></li>
             
                <li><a href="{{route('value.create')}}"><i class="iconsmind-Arrow-Through"></i>create</a></li> 
      
            </ul>
          
             
          </li>

          <li class="treeview">
              <a href="#">
                <i class="iconsmind-Notepad"></i>
                <span>Service Note</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-right pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{route('note.index')}}"><i class="iconsmind-Arrow-Through"></i>List</a></li>
               
                  <li><a href="{{route('note.create')}}"><i class="iconsmind-Arrow-Through"></i>create</a></li> 
        
              </ul>
            
               
            </li>
  

    <li class="treeview">
      <a href="#">
        <i class="iconsmind-Feather"></i>
        <span>Featured Content</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-right pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{route('featured.index')}}"><i class="iconsmind-Arrow-Through"></i>List</a></li>
       
          <li><a href="{{route('featured.create')}}"><i class="iconsmind-Arrow-Through"></i>create</a></li> 
     
  </ul>
    </li>

    <li class="treeview">
      <a href="#">
        <i class="iconsmind-Bebo"></i>
        <span>Blog</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-right pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{route('blog.index')}}"><i class="iconsmind-Arrow-Through"></i>List</a></li>
       
          <li><a href="{{route('blog.create')}}"><i class="iconsmind-Arrow-Through"></i>create</a></li> 
     
  </ul>
    </li>

    <li class="treeview">
      <a href="#">
        <i class="iconsmind-Address-Book"></i>
        <span>Contact</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-right pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{route('contact.index')}}"><i class="iconsmind-Arrow-Through"></i>Create</a></li>
       
     
  </ul>
    </li>

    <li class="treeview">
      <a href="#">
        <i class="iconsmind-Facebook-2"></i>
        <span>Social Media</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-right pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{route('social.index')}}"><i class="iconsmind-Arrow-Through"></i>Create</a></li>
       
     
  </ul>
    </li>

    <li class="treeview">
      <a href="#">
        <i class="iconsmind-File-Pictures"></i>
        <span>Logo</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-right pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{route('logo.index')}}"><i class="iconsmind-Arrow-Through"></i>Create</a></li>
       
     
  </ul>
    </li>

    <li class="treeview">
      <a href="#">
        <i class="iconsmind-File-Pictures"></i>
        <span>Copyright</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-right pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{route('copyright.index')}}"><i class="iconsmind-Arrow-Through"></i>Create</a></li>
       
     
  </ul>
    </li>


    </ul>



  </section>
</aside>