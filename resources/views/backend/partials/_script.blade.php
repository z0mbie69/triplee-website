	<!-- jQuery 3 -->
	<script src="{{asset('backend/assets/vendor_components/jquery/dist/jquery.js')}}"></script>
	
	<!-- popper -->
	<script src="{{asset('backend/assets/vendor_components/popper/dist/popper.min.js')}}"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="{{asset('backend/assets/vendor_components/bootstrap/dist/js/bootstrap.js')}}"></script>	
	
	<!-- Slimscroll -->
	<script src="{{asset('backend/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
	
	<!-- FastClick -->
	<script src="{{asset('backend/assets/vendor_components/fastclick/lib/fastclick.js')}}"></script>
	
	<!-- apexcharts -->
	<script src="{{asset('backend/assets/vendor_components/apexcharts-bundle/irregular-data-series.js')}}"></script>
	<script src="{{asset('backend/assets/vendor_components/apexcharts-bundle/dist/apexcharts.js')}}"></script>
	
	<!-- NeoX Admin App -->
	<script src="{{asset('backend/js/template.js')}}"></script>
	
	<!-- NeoX Admin dashboard demo (This is only for demo purposes) -->
	<script src="{{asset('backend/js/pages/dashboard3.js')}}"></script>
	
	<!-- NeoX Admin for demo purposes -->
	<script src="{{asset('backend/js/demo.js')}}"></script>



	<script src="{{asset('backend/assets/vendor_components/sweetalert/sweetalert.min.js')}}"></script>
	<script src="{{asset('backend/assets/vendor_components/sweetalert/jquery.sweet-alert.custom.js')}}"></script>
	
	<!-- ck editor -->
	<script src="{{asset('backend/assets/vendor_components/ckeditor/ckeditor.js')}}"></script>
<!-- {{-- <script src="{{asset('backend/assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js')}}"></script> --}} -->
{{-- <script src="{{asset('backend/assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js')}}"></script> --}}
    <script type="text/javascript"src="{{asset('backend/js/pages/editor.js')}}"></script>


	
<!-- multiple category selection-->

		<!-- Select2 -->
<script src="{{asset('backend/assets/vendor_components/select2/dist/js/select2.full.js')}}"></script>
	
	<!-- NeoX Admin for advanced form element -->
	<script src="{{asset('backend/js/pages/advanced-form-element.js')}}"></script>

	<!-- multiple category selection -->
	

	{{-- datatable --}}
	<script src="{{asset('backend/js/pages/data-table.js')}}"></script>
<script src="{{asset('backend/assets/vendor_components/datatable/datatables.min.js')}}"></script>
<script src="{{asset('backend/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('backend/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('backend/assets/vendor_components/fastclick/lib/fastclick.js')}}"></script>
<script src="{{asset('backend/assets/vendor_components/fastclick/lib/fastclick.js')}}"></script>

{{-- delete function --}}

<script>
    $(document).ready(function () {
        $(".deleteRecord").click(function(){
            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to revert this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-danger',
                    cancelButtonClass: 'btn btn-danger',
                    buttonStyling: false,
                    reverseButtons: true
                },
                function () {
                    window.location.href="/"+deleteFunction+"/"+id;

                });
        });
    });
</script>
