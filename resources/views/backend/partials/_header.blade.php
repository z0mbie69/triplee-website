
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
	<meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="icon" href="{{asset('backend/uploads/image/'.$logo_data->limage)}}" style="border-radius: 50% !important;"/>

   

    <title>Tripleee @yield('title')</title>
    

<!-- Bootstrap 4.0-->
<link rel="stylesheet" href="{{asset('backend/assets/vendor_components/bootstrap/dist/css/bootstrap.css')}}">

<!-- Bootstrap extend-->
<link rel="stylesheet" href="{{asset('backend/css/bootstrap-extend.css')}}">

<!-- theme style -->
<link rel="stylesheet" href="{{asset('backend/css/master_style.css')}}">

<!-- NeoX Admin skins -->
<link rel="stylesheet" href="{{asset('backend/css/skins/_all-skins.css')}}">

<link href="{{asset('backend/assets/vendor_components/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
 {{-- editor --}}
<link rel="stylesheet" href="{{asset('backend/assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css')}}">

<!-- Select2 -->
<link rel="stylesheet" href="{{asset('backend/assets/vendor_components/select2/dist/css/select2.min.css')}}">


<!-- multiple category selection -->

<!-- Select2 -->
    <link rel="stylesheet" href="{{asset('backend/assets/vendor_components/select2/dist/css/select2.min.css')}}">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{asset('backend/css/master_style.css')}}">

	

<!-- multiple category selection -->
	

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

     
  