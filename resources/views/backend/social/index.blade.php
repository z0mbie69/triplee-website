@extends('backend.main')

@section('content')

<div class="content-wrapper">
  <div class="content">
         <div class="box box-solid box-primary">
            <div class="box-header with-border">
              <h4 class="box-title">Social Media Data Table</h4>
              <h6 class="box-subtitle text-white-50">Export data to Copy, CSV, Excel, PDF & Print</h6>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
				  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                  <tr>
                    <th>S.N</th>  
                    <th>Facebook URL</th>
                    <th>Instagram URL</th>
                    <th>Twitter URL</th>
                    <th>Youtube URL</th>
                    <th>GitHub URL</th>
                   <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($social as $social)
                     <tr>
                       <td>{{$loop->index+1}}</td>
                       <td>{{$social->facebook_url}}</td>
                       <td>{!!$social->instagram_url!!}</td>
                       <td>{!!$social->twitter_url!!}</td>
                       <td>{!!$social->youtube_url!!}</td>
                       <td>{!!$social->github_url!!}</td>

                     
                       <td>
                          <a href="{{route('social.edit',$social->id)}}" class="icon"><i class="fa fa-edit fa-2x text-info"></i></a>&nbsp;&nbsp;
                                            
                         </td>

                     </tr>
                     @endforeach
                   </tbody>
				
				</table>
				</div>              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
</div>
</div>
@endsection










