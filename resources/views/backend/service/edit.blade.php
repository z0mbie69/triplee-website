@extends('backend.main')
@section('title','| Edit Service')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Service Form Edit
    </h1>
    <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="iconsmind-Library"></i></a></li>
      <li class="breadcrumb-item"><a href="{{route('service.index')}}">List all</a></li>
      <li class="breadcrumb-item active"><a href="{{route('service.create')}}">create</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
   
   <!-- Basic Forms -->
    <div class="box box-solid box-primary">
      <div class="box-header with-border">
        <h6 class="box-subtitle text-white">Service Edit Form</h6>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col">
            {{Form::model($service,['route'=>['service.update',$service->id],'method'=>'PUT','files'=>'true','data-parsley-validate'=>''])}}
      
            <div class="form-group">
                <h5>Title:<span class="text-danger">*</span></h5>
                <div class="controls">
                <Input type="text" name="title" value="{{$service->title}}"class="form-control"  placeholder="{{$service->title}}" required>
                </div>
              </div>


        <div class="form-group">
          <h5>Description: <span class="text-danger">*</span></h5>
          <div class="controls">
							<textarea name="description"  id="editor1" rows="4" cols="50" placeholder="Describe yourself here..." required data-validation-required-message="This field is required"> {{$service->description}}</textarea> </div>

        </div>

        <div class="form-group">
            <h5>Icon:<span class="text-danger">*</span></h5>
            <div class="controls">
              <Input type="text"  value="{{$service->icon}}"name="icon" class="form-control"  placeholder=" {{$service->icon}}" required>
               
            </div>
          </div>

      
            {{Form::submit('submit',['class'=>'form-control'])}}
            {{Form::close()}}
          </div>
        </div></div></div></section></div>
@endsection
