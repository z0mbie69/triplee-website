<?php

use Illuminate\Database\Seeder;

class AboutparasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        DB::table('aboutparas')->insert(array(
            array(
              'detail' => 'Migration and Seeder',
              'category' => 'aboutparagraph',
            ),
            array(
              'detail' => 'Adding multiple data in seeder',
              'category' => 'visionparagraph',
            ),
          ));
     
    
    }
}
