<?php

use Illuminate\Database\Seeder;
use App\Logo;

class LogoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $logo= new Logo();
        $logo->limage = "logo.png";
        $logo->name="EEE Innovation";
        $logo->save(); 
    }
}
