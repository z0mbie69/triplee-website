<?php

use Illuminate\Database\Seeder;
use App\Social;

class SocialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $social= new Social();
        $social->facebook_url = "facebook_url";
        $social->instagram_url = "instagram_url";
        $social->twitter_url = "twitter_url";
        $social->youtube_url = "youtube_url";
        $social->github_url = "github_url";
        $social->save(); 
    }
}
