<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(LogoTableSeeder::class);
         $this->call(ContactTableSeeder::class);
         $this->call(SocialTableSeeder::class);
         $this->call(AboutparasTableSeeder::class);
    }
}
