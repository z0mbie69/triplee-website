<?php

use Illuminate\Database\Seeder;
use App\Contact;

class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contact= new Contact();
        $contact->address1 = "address1";
        $contact->address2 = "address2";
        $contact->email = "email";
        $contact->contact1 = "contact1";
        $contact->contact2 = "contact2";
        $contact->save(); 
    }
}
