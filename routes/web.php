<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\User;
use Illuminate\Support\Facades\Input;

//Search
Route::get ( '/', function () {
    return view ( 'welcome' );
} );
Route::any ( '/search', function () {
    $q = Input::get ( 'q' );
    if($q !=''){
    $user = User::where ( 'name', 'LIKE', '%' . $q . '%' )->orWhere ( 'email', 'LIKE', '%' . $q . '%' )->get ();
    if (count ( $user ) > 0)
        return view ( 'welcome' )->withDetails ( $user )->withQuery ( $q );
    else
        return view ( 'welcome' )->withMessage ( 'No Details found. Try to search again !' );
}} );

Route::resource('front','FrontendController');
Route::get('/','FrontendController@index')->name('front.index');


//front pages
Route::get('/about','FrontendController@getAbout')->name('front.about');
Route::get('/service','FrontendController@getService')->name('front.service');
Route::get('/portfolio','FrontendController@getPortfolio')->name('front.portfolio');
Route::get('/ourteam','FrontendController@getOurteam')->name('front.ourteam');
Route::get('/contact','FrontendController@getContact')->name('front.contact');
Route::post('/save','FrontendController@saveform')->name('saveform');

Route::get('/blog','FrontendController@getBlog')->name('front.blog');

Route::get('/blog/{id}','FrontendController@blogShow')->name('fblog.show');

Route::get('/featured/{id}','FrontendController@featureshow')->name('feature.show');


Auth::routes();

Route::prefix('admin')->group(function(){

//backend dashboard
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/logout','Auth\LoginController@AdminLogout')->name('admin.logout');

//service
Route::resource('service','ServiceController');
Route::get('/service/destroy/{id}','ServiceController@destroy')->name('service.delete');

//For Team
Route::resource('team','TeamController');
Route::get('/team/destroy/{id}','TeamController@destroy')->name('team.delete');


//For Article
Route::resource ('slider','SliderController');
Route::get('/slider/destroy/{id}','SliderController@destroy')->name('slider.delete');

//For Featured Content
Route::resource('featured','FeaturedController');
Route::get('/featured/destroy/{id}','FeaturedController@destroy')->name('featured.delete');


//For Blog
Route::resource('blog','BlogController');
Route::get('/blog/destroy/{id}','BlogController@destroy')->name('blog.delete');


//slider
Route::resource ('slider','SliderController');
Route::get('/slider/destroy/{id}','SliderController@destroy')->name('slider.delete');

//mslider
Route::resource ('mslider','MsliderController');
Route::get('/mslider/destroy/{id}','MsliderController@destroy')->name('mslider.delete');

//Alltitle
Route::resource ('alltitle','AlltitleController');
Route::get('/alltitle/destroy/{id}','AlltitleController@destroy')->name('alltitle.delete');

///about paragraph
Route::get('/aboutpara','AboutController@paraindex')->name('aboutpara.index');
Route::get('/aboutpara/create','AboutController@paracreate')->name('aboutpara.create');
Route::post('/aboutpara','AboutController@parastore')->name('aboutpara.store');
Route::get('/aboutpara/destroy/{id}','AboutController@paradestroy')->name('aboutpara.destroy');
Route::get('/aboutpara/{id}/edit','AboutController@paraedit')->name('aboutpara.edit');
Route::put('/aboutpara/{id}','AboutController@paraupdate')->name('aboutpara.update');

//about numeric
Route::resource('numeric','about\NumericController');
Route::get('/numeric/destroy/{id}','about\NumericController@destroy')->name('numeric.delete');

//about approach
Route::resource('approach','about\ApproachController');
Route::get('/approach/destroy/{id}','about\ApproachController@destroy')->name('approach.delete');

//abutton about
Route::resource('abutton','about\AbuttonController');
Route::get('/abutton/destroy/{id}','about\AbuttonController@destroy')->name('abutton.delete');

//service value
Route::resource('value','Service\ValuesController');
Route::get('/value/destroy/{id}','Service\ValuesController@destroy')->name('value.delete');

//Note service
Route::resource('note','Service\NoteController');
Route::get('/note/destroy/{id}','Service\NoteController@destroy')->name('note.delete');

//for contact
Route::resource('contact','ContactController');
Route::get('/contact/destroy/{id}','ContactController@destroy')->name('contacts.delete');

//for Social Media
Route::resource('social','SocialController');
Route::get('/social/destroy/{id}','SocialController@destroy')->name('social.delete');

//for logo
Route::resource('logo','LogoController');

//for Copyright
Route::resource('copyright','CopyrightController');

});



